<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ReservedRoom extends \Eloquent{
//	relationship
	public function client(){
		return $this->belongsTo(Client::class);
	}

	public function rooms(){
		return $this->belongsToMany(Room::class);
	}
//	functions
	public function getFullName(){
		$firstName = $this->client->first_name;
		$lastName = $this->client->last_name;
		$fullName = $firstName.' '.$lastName;
		return $fullName;
	}

	public static function disponibleRooms(){
		$query = ReservedRoom::orderBy('id', 'desc');

		// start_date = 20 |  end_date = 24
		// You have 6 cases: 19<20<22 | 22<24<26 | 20<21<23<24 | 20 = 20 && 24 =24 |  20 = 20 && 24> 22 | 20<25 && 24=24
		if(request()->filled('date')){
			$fullDate = request('date');
			list($startDate, $endDate) = explode(' to ', $fullDate);
		}else{
			// Rooms available this week
			$startDate = Carbon::now()->toDateString();
			$endDate = Carbon::now()->addWeek()->toDateString();
		}

		$query->where([['start_date', '>', $startDate], ['start_date', '<', $endDate]]);
		$query->orWhere([['end_date', '>', $startDate], ['end_date', '<', $endDate]]);
		$query->orWhere([['start_date', '<=', $startDate], ['end_date', '>=', $endDate]]);

		$reservedRooms = $query->get();
		$roomIds = [];

		foreach ($reservedRooms as $reservedRoom){
			foreach ($reservedRoom->rooms as $room){
				$roomIds[] = $room->id;
			}
		}


		$availableRooms = Room::whereNotIn('id', $roomIds)->get();
		//dd($startDate, $endDate, $reservedRooms,$availableRooms);

		return $availableRooms;
	}
}
