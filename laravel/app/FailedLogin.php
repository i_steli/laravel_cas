<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FailedLogin extends \Eloquent{
	public static function failedLogins($minutes=0, $hours=0){
		$limitTime = Carbon::now()->subMinutes($minutes)->subHours($hours);
		$countFailedLogins = FailedLogin::where('ip', request()->ip())
		                                ->where('created_at', '>', $limitTime)
		                                ->count();

		return $countFailedLogins;
	}
}
