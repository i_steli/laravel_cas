<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends \Eloquent{

	// relationships
	public function galleries() {
		return $this->belongsToMany( Gallery::class );
	}

	public function reservedRooms(){
		return $this->belongsToMany(ReservedRoom::class);
	}

	protected $fillable = [ 'name', 'price', 'description', 'currency' ];

	// functions
	public static function getCurrencies() {
		return [
			'lei'    => 'ron',
			'euro'   => 'eur',
			'dollar' => 'usd'
		];
	}

	public static function getAdults() {
		return [
			1,
			2,
			3,
			4,
		];
	}

	public static function getChildren() {
		return [
			1,
			2,
			3,
		];
	}

	public function hasGallery($galleryId) {
		//1 $this poate fi folosit va lua roomul curent, nu mai e nevoie sa faci instanta si sa preiei modelul
		//2 in view avand deja modelul $room care apartine modelului Room, nu mai e nevoie sa faci o instanta sau sa creezi o functie statica $room->selectGallery
		//3 room->galleries cauta in tabela pivot
		$galleryInRoom = $this->galleries()->where( 'gallery_id', $galleryId )->get()->first();
		return $galleryInRoom != null ? true : false;
	}

	public function getAdultPrice(){
		return ($this->price_adults*$this->initial_price)/100 + $this->initial_price;
	}

	public function getChildrenPrice(){
		return ($this->price_children*$this->initial_price)/100 + $this->initial_price;
	}
}
