<?php

namespace App\Mailers;

class ForgetPasswordMailer extends Mailer{
	public function mailForgetPassword($email, $token){
		$user = $email;
		$view = 'cpanel.users.templates.forget-password';

		$data['subject'] = 'Forget Password';
		$data['setForm'] = 'no-reply@yahoo.com';
		$data['setFormName'] = 'Test';
		$data['addAddress'] = ['stelian@axelsoft.ro', 'ivan.stelian@axelsoft.ro'];
		$data['addReplyTo'] = 'no-reply@yahooooo.com';
		$data['addReplyToName'] = 'Nobody';
		$data['token'] = $token;
		$data['email'] = $email;

		/** @noinspection PhpVoidFunctionResultUsedInspection */
		return $this->sendTo($user, $view, $data);
	}
}