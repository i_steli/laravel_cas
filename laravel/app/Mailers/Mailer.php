<?php

namespace App\Mailers;
use PHPMailer\PHPMailer\PHPMailer;
use View;

abstract class Mailer{
	// proprieties
	private $embeddedImages  = [];
	private $key = 0;
	//	Server Settings
	public static function make(){
		$mail = new PHPMailer();

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;
		//Set the hostname of the mail server
		$mail->Host = "smtp.mailtrap.io";
		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = 465;
		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = "4470407af68373";
		//Password to use for SMTP authentication
		$mail->Password = "bdadfb7d98ac94";

		return $mail;
	}

	public function setEmbedded($path, $variable, $name){
		$this->key = $this->key + 1;
		$this->embeddedImages[$this->key]['path'] = $path;
		$this->embeddedImages[$this->key]['variable'] = $variable;
		$this->embeddedImages[$this->key]['name'] = $name;

		return $this->embeddedImages;
	}

	public function getEmbedded(){
		return $this->embeddedImages;
	}

	public function sendTo($user, $view, $data = array()){
		$mail = self::make();
		//dd($this->getEmbedded());
		// optional
		$data['setFormName'] = isset($data['setFormName']) ? $data['setFormName'] : '';
		$data['addReplyToName'] = isset($data['addReplyToName']) ? $data['addReplyToName'] : '';
		//Recipients
		if(isset($user)){
			$mail->addAddress($user);
		}

		// makes automatic validate of the address
		if(isset($data['setForm'])){
			$mail->setFrom($data['setForm'], $data['setFormName']);
		}

		// add Reply
		if(isset($data['addReplyTo'])){
			$mail->addReplyTo($data['addReplyTo'], $data['addReplyToName']);
		}

		// subject
		if(isset($data['subject'])){
			$mail->Subject = $data['subject'];
		}else{
			$mail->Subject = 'PhpMailer Test';
		}

		// addAddress
		if(isset($data['addAddress']) && is_string($data['addAddress'])){
			$mail->addAddress($data['addAddress']);
		}

		if(isset($data['addAddress']) && is_array($data['addAddress'])){
			foreach ($data['addAddress'] as $email){
				$mail->addAddress($email);
			}
		}

		// addBcc
		if(isset($data['addBCC']) && is_string($data['addBCC'])){
			$mail->addBCC($data['addBCC']);
		}

		if(isset($data['addBCC']) && is_array($data['addBCC'])){
			foreach ($data['addBCC'] as $email){
				$mail->addBCC($email);
			}
		}

		// addCC
		if(isset($data['addCC']) && is_string($data['addCC'])){
			$mail->addCC($data['addCC']);
		}

		if(isset($data['addCC']) && is_array($data['addCC'])){
			foreach ($data['addCC'] as $email){
				$mail->addCC($email);
			}
		}

		// add Attachment
		if(isset($data['attachment']) && is_string($data['attachment'])){
			$mail->addAttachment($data['attachment']);
		}

		if(isset($data['attachment']) && is_array($data['attachment'])){
			foreach ($data['attachment'] as $attachment){
				$mail->addAttachment($attachment);
			}
		}

		// add Embedded Picture
		if(count($this->getEmbedded()) > 0){
			foreach ($this->getEmbedded() as $image){
				$mail->addEmbeddedImage(base_path('../public_html/').$image['path'], $image['variable'], $image['name']);
				// set varable for data so we can call it with blade and without cid
				$data[$image['variable']] = 'cid:'.$image['variable'];
			}
		}

		// Render the html with with blade
		$mail->msgHTML(View::make($view, $data)->render());

		return $mail->send() ? true : false;
	}

}