<?php

namespace App;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

//    realtionship
	public function roles(){
		return $this->belongsToMany(Role::class);
	}

	public function logs(){
		return $this->hasMany(LogUserAction::class);
	}

	public function sessions() {
		return $this->hasMany( SessionData::class );
	}

//	function
	public function hasRole($roleId){
		return $this->roles()->where('role_id', $roleId)->get()->first() != null ? 'checked' : '';
	}

	public static function toggleOrder($col=''){
		request('order') == 'asc' && request('col') == $col  ? $order = 'desc' : $order = 'asc';
		return array_merge(request()->all(), ['col'=>$col, 'order'=>$order]);
	}

	public static function toggleFilter($filter, $attach = array()){
		$attachParameters = request()->only($attach);
		return request($filter) == 1 ? array_merge($attachParameters) : array_merge($attachParameters, [$filter=>'1']);

	}

	public function hasPermission($permissions, $requiredAll = false){
		// get all the permissions from the roles attached to the current user
		$userRoles = $this->roles;
		$rolePermissions = [];
		foreach($userRoles as $role){
			foreach ($role->permissions as $rolePermission){
				$rolePermissions[] = $rolePermission->name;
			}
		}

		// make unique, roles can have the same permissions
		$rolePermissions = array_unique($rolePermissions);
		// check if user is admin
		if($this->isAdmin($rolePermissions))  return true;
		// check the user

		// check for permissions
		if(is_string($permissions)){
			return in_array($permissions, $rolePermissions);
		}

		$intersection = count(array_intersect($permissions, $rolePermissions));
		// check if all the permisions sent from the navigation are included in user permissions
		if($requiredAll){
			return $intersection == count($permissions) ? true : false;
		}
		// check if we have at least 1 permission sent from the navigation which is included in user permisions
		return $intersection > 0 ? true : false;
	}

	public function isAdmin($rolePermissions){
		return in_array('admin', $rolePermissions);
	}

	public static function setImpersonating($id){
		Session::put('impersonate', $id);
	}

	public static function stopImpersonating(){
		Session::forget('impersonate');
	}

}
