<?php

namespace App;

class Role extends \Eloquent{
	//    realtionship
	public function users(){
		return $this->belongsToMany(User::class);
	}

	public function permissions(){
		return $this->belongsToMany(Permission::class);
	}
	// functions
	public function hasPermisson($permissionId){
		$roleHasPermissions =  $this->permissions()->where('permission_id', $permissionId)->get()->first();
		return $roleHasPermissions != null ? true : false;
	}

}
