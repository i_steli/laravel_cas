<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends \Eloquent{
//	relationship
	public function reservedRooms(){
		return $this->hasMany(ReservedRoom::class);
	}
}
