<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionData extends \Eloquent{
	protected $table = 'sessions';
	// relationship
	public function user() {
		return $this->belongsTo( User::class );
	}

}
