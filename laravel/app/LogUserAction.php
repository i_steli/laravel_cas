<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class LogUserAction extends \Eloquent{
    //relationship
	public function user(){
		return $this->belongsTo(User::class);
	}

    public static function storeAction($action = 'not set', $description = array('not set')){
		$storeAction = new LogUserAction();
	    $storeAction->user_id = Auth::user()->id;
	    $storeAction->action = $action;
	    $storeAction->description = json_encode($description);
	    $storeAction->save();
    }

    public function changeJson(){
		$decodeJson = json_decode($this->description);
	    return json_encode($decodeJson, JSON_PRETTY_PRINT);
    }

    public static function getUrl($currentUrl, $except = array(), $addQueries = array()){
    	$queries = array_merge(request()->except($except), $addQueries);
    	// check if we have queries
    	if(count($queries) == 0) return $currentUrl;

	    $currentUrl .= '?';
    	foreach ($queries as $var => $val){
		    $currentUrl .= $var."=".$val."&";
	    }

	    return rtrim($currentUrl, "&");
    }

}
