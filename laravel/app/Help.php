<?php namespace App;

//use Illuminate\Routing\Route;
//use Illuminate\Support\Facades\Route;
use Route;

class Help{

	public static function convertBytes($bytes){
		$arrayCoverters = ['Bytes', 'KB', 'MB', 'GB'];
		$converted = $bytes;
		foreach ($arrayCoverters as $arrayCoverter){
			if($converted < 1024){
				$converted = round($converted, 2).' '.$arrayCoverter;
				break;
			}
			$converted /= 1024;
		}

		return $converted;
	}

	public function hasRoute($categories = null){
		if(is_array($categories)){
			return in_array(Route::currentRouteName(), $categories);
		}

		return Route::currentRouteName() == $categories ? true : false;
	}

}
