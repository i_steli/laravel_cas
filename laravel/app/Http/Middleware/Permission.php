<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Redirect;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
	    if(! Auth::user()) return Redirect::route('login')->with('errorMessage', 'Nu sunteti logat');
	    if(! Auth::user()->hasPermission($permission)) return Redirect::route('dashboard.index')->with('errorMessage', 'Contul dumneavoastra nu are permisiuni: '.$permission);

	    return $next($request);
    }
}
