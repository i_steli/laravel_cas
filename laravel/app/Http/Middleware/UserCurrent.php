<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Redirect;

class UserCurrent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	// $request gets the parameter from the route /users/{id}/profile
	    if(! Auth::user()) return Redirect::route('login')->with('errorMessage', 'Nu sunteti logat');
	    if( Auth::user()->id != $request->id  && ! Auth::user()->hasPermission('admin')) return Redirect::route('dashboard.index')->with('errorMessage', 'Nu puteti accesa contul altei persoane');
        return $next($request);
    }
}
