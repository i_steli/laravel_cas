<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Room;
use Validator;

class RoomController extends Controller{

    public function index(){
    	$perPage = request()->filled('perPage') ? request('perPage') : 2;
	    $query = Room::orderBy('id', 'desc');

    	if(request()->filled('search')){
		    $searchId = str_replace('#', 'LIKE', request('search'));
		    $query
			    ->where('id', '=', $searchId)
			    ->orWhere('name', 'LIKE', '%' . request('search') . '%');
	    }

		$rooms = $query->paginate($perPage);
	    return view('cpanel.rooms.index')->with('rooms', $rooms);
	    //return view('cpanel.rooms.index');
    }


    public function create(){
		return view('cpanel.rooms.create');
    }

    public function store(){
		$rules = array(
			'name'=>'required',
			'adults'=>'required|numeric',
			'initial_price'=>'required|numeric',
			'currency'=>'required',
			'price_adults'=>'required|numeric',
			'price_children'=>'numeric',
			'promotion'=>'numeric',
			'set_age'=>'numeric'
		);

		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

	    $room = new Room();
		$room->name = request('name');
	    $room->adults = request('adults');
	    $room->initial_price = request('initial_price');
	    $room->max_adults = request('max_adults');
	    $room->price_adults = request('price_adults');
	    $room->children = request('children');
	    $room->set_age = request('set_age');
	    $room->price_children = request('price_children');
	    $room->promotion = request('promotion');
	    $room->currency = request('currency');
	    $room->description = request('description');
	    $room->save();


	    return redirect()->route('rooms.index')->with('successMessage', 'Room has been created.');
    }

    public function edit($id){
		$room = Room::where('id', $id)->get()->first();
		if($room == null){
			return back()->with('errorMessage', 'Room not found');
		}

		$galleries = Gallery::all();

		return view('cpanel.rooms.edit')
			->with('room', $room)
			->with('galleries', $galleries);
    }

    public function update($id){
	    $rules = array(
		    'name'=>'required',
		    'adults'=>'required|numeric',
		    'initial_price'=>'required|numeric',
		    'currency'=>'required',
		    'price_adults'=>'required|numeric',
		    'price_children'=>'numeric',
		    'promotion'=>'numeric',
		    'set_age'=>'numeric'
	    );

	    $validator = Validator::make(request()->all(), $rules);
	    if($validator->fails()){
		    return back()->withInput()->withErrors($validator);
	    }

	    $room = Room::where('id', $id)->get()->first();
	    if($room == null){
		    return back()->with('errorMessage', 'Room not found');
	    }

	    $room->name = request('name');
	    $room->adults = request('adults');
	    $room->initial_price = request('initial_price');
	    $room->max_adults = request('max_adults');
	    $room->price_adults = request('price_adults');
	    $room->children = request('children');
	    $room->set_age = request('set_age');
	    $room->price_children = request('price_children');
	    $room->promotion = request('promotion');
	    $room->currency = request('currency');
	    $room->description = request('description');
	    $room->save();

	    return back()->with('successMessage', 'Room has been updated');

    }

    public function destroy(){
		$roomIds = request('roomIds');
		//dd($roomIds);
		if($roomIds == null){
			return back()->with('errorMessage', 'Please select a field');
		}
		//Room::destroy($roomIds);
	    Room::whereIn('id', $roomIds)->delete();
		return back()->with('successMessage', 'Fields deleted');
    }

    public function addGallery(){
    	$roomGalleryIds = request('roomGalleries');

    	$roomId = request('roomId');
	    if($roomId == null){
		    return back()->with('errorMessage', 'Room not found');
	    }
		$room = Room::where('id', $roomId)->get()->first();
	    $galleryAllIds = Gallery::get(['id']);

	    $room->galleries()->detach($galleryAllIds);
	    $room->galleries()->attach($roomGalleryIds);

	    return back()->with('successMessage', 'Fields has been updated');
    }


}
