<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Image;
use Illuminate\Http\Request;
use Validator;

class GalleryController extends Controller{

    public function index(){
	    $perPage = request()->filled('perPage') ? request('perPage') : 2;
	    $query = Gallery::orderBy('id', 'desc');

	    if(request()->filled('search')){
		    $searchId = str_replace('#', 'LIKE', request('search'));
		    $query
			    ->where('id', '=', $searchId)
			    ->orWhere('name', 'LIKE', '%' . request('search') . '%');
	    }

	    $galleries = $query->paginate($perPage);
	    return view('cpanel.galleries.index')->with('galleries', $galleries);
	    //return view('cpanel.rooms.index')
    }


    public function store(){
	    $rules = array(
		    'name'=>'required',
	    );

	    $validator = Validator::make(request()->all(), $rules);
	    if($validator->fails()){
		    return back()->withInput()->withErrors($validator);
	    }

	    $gallery = new Gallery();
	    $gallery->name = request('name');
	    $gallery->save();

	    return back()->with('successMessage', 'Gallery has been created.');
    }

    public function edit($id){
		$gallery = Gallery::where('id', $id)->get()->first();
		if($gallery == null){
			return back()->with('errorMessage', 'Gallery not found');
		}

	    $perPage = request()->filled('perPage') ? request('perPage') : 3;

	    $query = Image::orderBy('id', 'desc');
		if(request()->filled('search')){
			$strReplace = str_replace('#', '', request('search'));
			$query->where('id', '=',$strReplace);
			$query->orWhere('name', 'LIKE' ,'%'. request('search') .'%');
		}
		$images = $query->paginate($perPage);

		return view('cpanel.galleries.edit')
			->with('gallery', $gallery)
			->with('images', $images);
    }

    public function update($id){
	    $rules = array(
		    'name'=>'required',
	    );

	    $validator = Validator::make(request()->all(), $rules);
	    if($validator->fails()){
		    return back()->withInput()->withErrors($validator);
	    }

	    $gallery = Gallery::where('id', $id)->get()->first();
	    if($gallery == null){
		    return back()->with('errorMessage', 'Gallery not found');
	    }

	    $gallery->name = request('name');
	    $gallery->save();

	    return back()->with('successMessage', 'Gallery has been updated.');

    }

    public function destroy(){
	    $galleryIds = request('galleryIds');
	    if($galleryIds == null){
		    return back()->with('errorMessage', 'Please select a field');
	    }
	    //Room::destroy($galleryIds);
	    Gallery::whereIn('id', $galleryIds)->delete();
	    return back()->with('successMessage', 'Fields deleted');
    }

    public function addImage($id){
		$imageIds = request('imageIds');
		$gallery = Gallery::where('id', $id)->get()->first();
		if($gallery == null){
			return back()->with('errorMessage', 'Gallery not found');
		}

		// daca nu treci niciun argument in detach va face la tot
		$gallery->images()->detach();
		$gallery->images()->attach($imageIds);

	    return back()->with('successMessage', 'Images have been added');
    }

    public function removeImage($id){
		$gallery = Gallery::where('id', $id)->get()->first();
		$image = Image::where('id', request('imageId'))->get()->first();

	    if($gallery == null){
		    return back()->with('errorMessage', 'Gallery not found');
	    }

		$gallery->images()->detach($image);

	    return back()->with('successMessage', 'Images have been removed');
    }
}
