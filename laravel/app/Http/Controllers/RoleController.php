<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Validator;

class RoleController extends Controller{
	public function index(){
		$query = Role::orderBy('id', 'desc');
		$page = request()->has('perPage') ? request('perPage') : 5;
		if(request()->filled('search')){
			$replaceId = str_replace('#', '', request('search'));
			$query->where('id', '=', $replaceId);
			$query->orWhere('display_name', 'LIKE', '%' . request('search') . '%');
			$query->orWhere('name', 'LIKE', '%' . request('search') . '%');
		}

		$roles = $query->paginate($page);
		return view('cpanel.roles.index')->with('roles', $roles);
	}

	public function create(){
		return view('cpanel.roles.create');
	}

	public function store(){
		$rules = [
			'name'=>'required',
			'display_name'=>'required',
		];

		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		$role = new Role();
		$role->name = request('name');
		$role->display_name = request('display_name');
		$role->description = request('description');
		$role->save();

		return redirect()->route('roles.index')->with('successMessage', 'Role has been created');
	}

	public function edit($id){
		$role = Role::where('id', $id)->get()->first();
		$permissions = Permission::all();
		if($role == null){
			return back()->with('errorMessage', 'Role not found');
		}

		return view('cpanel.roles.edit')
			->with('role', $role)
			->with('permissions', $permissions);
	}

	public function update($id){
		$role = Role::where('id', $id)->get()->first();
		if($role == null){
			return back()->with('errorMessage', 'Role not found');
		}

		$rules = [
			'name'=>'required',
			'display_name'=>'required',
		];
		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		$role->name = request('name');
		$role->display_name = request('display_name');
		$role->description = request('description');
		$role->save();

		return back()->with('successMessage', 'Role has been updated');
	}

	public function destroy(){
		$roleIds = request('roleIds');
		if($roleIds == null){
			return back()->with('errorMessage', 'Please select fields');
		}

		Role::destroy($roleIds);

		return back()->with('successMessage', 'Fields has been deleted');
	}

	public function attachPermissions($id){
		$role = Role::where('id', $id)->get()->first();
		if($role == null){
			return back()->with('errorMessage', 'Role not found');
		}

		$role->permissions()->detach();
		$permisionIds = request('rolePermissions');
		$role->permissions()->attach($permisionIds);

		return back()->with('successMessage', 'Role has been updated');
	}
}
