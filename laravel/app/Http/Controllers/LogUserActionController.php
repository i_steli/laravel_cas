<?php

namespace App\Http\Controllers;

use App\LogUserAction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LogUserActionController extends Controller{
	public function index(){
		$perPage = request()->filled('perPage') ? request('perPage') : 5;
		$query = LogUserAction::orderBy('id', 'desc');
		if(request()->filled('search')){
			$stringReplace = str_replace('#', '', request('search'));
			$query->where('id', $stringReplace);
			$query->orWhere('action', 'LIKE', '%' . request('search'). '%');
			$query->orWhereHas('user', function($query){
				$query->where('name', 'LIKE', '%' . request('search'). '%');
			});
		}


		if(request()->has('start_date') || request()->has('end_date')){
			$startDate = request()->filled('start_date') ? request('start_date') : Carbon::now();
			$endDate = request()->filled('end_date') ? request('end_date') : Carbon::now();
			// Filter dupa data iti face intervale inchise cu ajutorul lui startofday si endofday
			$query->where('created_at', '>=', Carbon::parse($startDate)->startOfDay()->toDateTimeString());
			$query->where('created_at', '<=', Carbon::parse($endDate)->endOfDay()->toDateTimeString());
		}

		$logs = $query->paginate($perPage);
		return view('cpanel.logs.index')->with('logs', $logs);
	}

	public function destroy(){
		$logIds = request('logIds');
		if($logIds == null){
			return back()->with('errorMessage', 'Please select a field');
		}

		LogUserAction::whereIn('id', $logIds)->delete();
		return back()->with('successMessage', 'Log users were deleted');
	}

}
