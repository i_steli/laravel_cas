<?php

namespace App\Http\Controllers;

use App\Client;
use App\ReservedRoom;
use Illuminate\Support\Facades\DB;

class ReservedRoomController extends Controller{
	public function index(){
		$perPage = request()->filled('perPage') ? request('perPage') : 3;
		$query = ReservedRoom::orderBy('id', 'desc');

		if(request()->filled('search')){
			$searchId = str_replace('#', 'LIKE', request('search'));
			$query
				->where('id', '=', $searchId)
				->orWhereHas('rooms', function($query){
					$query->where('name', 'LIKE', '%' . request('search') . '%');
				})
				->orWhereHas('client', function($query){
					$query->where('first_name', 'LIKE', '%' . request('search') . '%');
				})
				->orWhereHas('client', function($query){
					$query->where('last_name', 'LIKE', '%' . request('search') . '%');
				});
			}

		$reservedRooms = $query->paginate($perPage);
		return view('cpanel.reserved-rooms.index')->with('reservedRooms', $reservedRooms);
		//return view('cpanel.rooms.index');
	}


	public function create(){
		$clients = Client::all();
		$availableRooms = ReservedRoom::disponibleRooms();

		return view('cpanel.reserved-rooms.create')
			->with('clients', $clients)
			->with('availableRooms', $availableRooms);
	}

	//
	//public function store(){
	//	$rules = array(
	//		'name'=>'required',
	//		'adults'=>'required|numeric',
	//		'initial_price'=>'required|numeric',
	//		'currency'=>'required',
	//		'price_adults'=>'required|numeric',
	//		'price_children'=>'numeric',
	//		'promotion'=>'numeric',
	//		'set_age'=>'numeric'
	//	);
	//
	//	$validator = Validator::make(request()->all(), $rules);
	//	if($validator->fails()){
	//		return back()->withInput()->withErrors($validator);
	//	}
	//
	//	$room = new ReservedRoom();
	//	$room->name = request('name');
	//	$room->adults = request('adults');
	//	$room->initial_price = request('initial_price');
	//	$room->max_adults = request('max_adults');
	//	$room->price_adults = request('price_adults');
	//	$room->children = request('children');
	//	$room->set_age = request('set_age');
	//	$room->price_children = request('price_children');
	//	$room->promotion = request('promotion');
	//	$room->currency = request('currency');
	//	$room->description = request('description');
	//	$room->save();
	//
	//
	//	return redirect()->route('rooms.index')->with('successMessage', 'ReservedRoom has been created.');
	//}
	//
	//public function edit($id){
	//	$room = ReservedRoom::where('id', $id)->get()->first();
	//	if($room == null){
	//		return back()->with('errorMessage', 'ReservedRoom not found');
	//	}
	//
	//	$galleries = Gallery::all();
	//
	//	return view('cpanel.rooms.edit')
	//		->with('room', $room)
	//		->with('galleries', $galleries);
	//}
	//
	//public function update($id){
	//	$rules = array(
	//		'name'=>'required',
	//		'adults'=>'required|numeric',
	//		'initial_price'=>'required|numeric',
	//		'currency'=>'required',
	//		'price_adults'=>'required|numeric',
	//		'price_children'=>'numeric',
	//		'promotion'=>'numeric',
	//		'set_age'=>'numeric'
	//	);
	//
	//	$validator = Validator::make(request()->all(), $rules);
	//	if($validator->fails()){
	//		return back()->withInput()->withErrors($validator);
	//	}
	//
	//	$room = ReservedRoom::where('id', $id)->get()->first();
	//	if($room == null){
	//		return back()->with('errorMessage', 'ReservedRoom not found');
	//	}
	//
	//	$room->name = request('name');
	//	$room->adults = request('adults');
	//	$room->initial_price = request('initial_price');
	//	$room->max_adults = request('max_adults');
	//	$room->price_adults = request('price_adults');
	//	$room->children = request('children');
	//	$room->set_age = request('set_age');
	//	$room->price_children = request('price_children');
	//	$room->promotion = request('promotion');
	//	$room->currency = request('currency');
	//	$room->description = request('description');
	//	$room->save();
	//
	//	return back()->with('successMessage', 'ReservedRoom has been updated');
	//
	//}

	public function destroy(){
		$reservedRoomIds= request('reservedRoomIds');
		//dd($roomIds);
		if($reservedRoomIds== null){
			return back()->with('errorMessage', 'Please select a field');
		}
		//ReservedRoom::destroy($roomIds);
		ReservedRoom::whereIn('id', $reservedRoomIds)->delete();
		return back()->with('successMessage', 'Fields deleted');
	}

	public function confirmation($id){
		$reservedRoom = ReservedRoom::where('id', $id)->get()->first();
		if($reservedRoom == null){
			return back()->with('errorMessage', 'Reserved Room not found');
		}
		$confirmation = request('confirm');

		$reservedRoom->confirmation = $reservedRoom->confirmation == $confirmation ? 0 : 1;
		$reservedRoom->save();
		return back()->with('successMessage', 'Room confirmed');
	}

	public function storeClient(){
		$intervalDate = request('intervalDate');
		if($intervalDate == ""){
			return back()->withInput()->with('errorMessage', 'Please select date');
		}
		list($startDate, $endDate) = explode(' to ', $intervalDate);

		$availableRoomId = request('availableRoom');
		if($availableRoomId == ""){
			return back()->withInput()->with('errorMessage', 'Please select a room');
		}

		$clientId = request('client');
		if($clientId == ""){
			return back()->withInput()->with('errorMessage', 'Please select a client');
		}

		/** @noinspection PhpUndefinedClassInspection */
		DB::beginTransaction();
		$reservedRoom = new ReservedRoom();
		$reservedRoom->client_id = $clientId;
		$reservedRoom->start_date = $startDate;
		$reservedRoom->end_date = $endDate;
		$reservedRoom->confirmation = true;
		$reservedRoom->save();

		$reservedRoom->rooms()->attach($availableRoomId);
		/** @noinspection PhpUndefinedClassInspection */
		DB::commit();
		return back()->with('successMessage', 'Room Reserved');
	}

	//public function disponibleRooms(){
	//		//dd('dispo');
	//		$fullDate = request('date');
	//		list($startDate, $endDate) = explode(' to ', $fullDate);
	//		$query = ReservedRoom::orderBy('id', 'desc');
	//
	//		// start_date = 20 |  end_date = 24
	//		// You have 6 cases: 19<20<22 | 22<24<26 | 20<21<23<24 | 20 = 20 && 24 =24 |  20 = 20 && 24> 22 | 20<25 && 24=24
	//		if(request()->filled('date')){
	//			$query->where([['start_date', '>', $startDate], ['start_date', '<', $endDate]]);
	//			$query->orWhere([['end_date', '>', $startDate], ['end_date', '<', $endDate]]);
	//			$query->orWhere([['start_date', '<=', $startDate], ['end_date', '>=', $endDate]]);
	//		}
	//		$reservedRooms = $query->get();
	//		$roomIds = [];
	//
	//		foreach ($reservedRooms as $reservedRoom){
	//			foreach ($reservedRoom->rooms as $room){
	//				$roomIds[] = $room->id;
	//			}
	//		}
	//
	//
	//		$availableRooms = Room::whereNotIn('id', $roomIds)->get();
	//		//dd($fullDate, $startDate, $endDate, $roomIds, $availableRooms);
	//		return back()->with('availableRooms', $availableRooms);
	//
	//}
}
