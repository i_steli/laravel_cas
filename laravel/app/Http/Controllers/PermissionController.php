<?php

namespace App\Http\Controllers;

use App\Permission;
use Validator;

class PermissionController extends Controller{
	public function index(){
		$query = Permission::orderBy('id', 'desc');
		$page = request()->has('perPage') ? request('perPage') : 5;
		if(request()->filled('search')){
			$replaceId = str_replace('#', '', request('search'));
			$query->where('id', '=', $replaceId);
			$query->orWhere('display_name', 'LIKE', '%' . request('search') . '%');
			$query->orWhere('name', 'LIKE', '%' . request('search') . '%');
		}

		$permissions = $query->paginate($page);
		return view('cpanel.permissions.index')->with('permissions', $permissions);
	}

	public function create(){
		return view('cpanel.permissions.create');
	}

	public function store(){
		$rules = [
			'name'=>'required',
			'display_name'=>'required',
		];

		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		$permission = new Permission();
		$permission->name = request('name');
		$permission->display_name = request('display_name');
		$permission->description = request('description');
		$permission->save();

		return redirect()->route('permissions.index')->with('successMessage', 'Permission has been created');
	}

	public function edit($id){
		$permission = Permission::where('id', $id)->get()->first();
		if($permission == null){
			return back()->with('errorMessage', 'Permission not found');
		}

		return view('cpanel.permissions.edit')->with('permission', $permission);
	}

	public function update($id){
		$permission = Permission::where('id', $id)->get()->first();
		if($permission == null){
			return back()->with('errorMessage', 'Permission not found');
		}

		$rules = [
			'name'=>'required',
			'display_name'=>'required',
		];
		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		$permission->name = request('name');
		$permission->display_name = request('display_name');
		$permission->description = request('description');
		$permission->save();

		return back()->with('successMessage', 'Permission has been updated');
	}

	public function destroy(){
		$permissionIds = request('permissionIds');
		if($permissionIds == null){
			return back()->with('errorMessage', 'Please select fields');
		}

		Permission::destroy($permissionIds);

		return back()->with('successMessage', 'Fields has been deleted');
	}
}
