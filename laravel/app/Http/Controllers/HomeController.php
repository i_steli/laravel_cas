<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;

class HomeController extends Controller{
    public function index(){
    	return view('home');
    }

    public function rooms(){
    	$rooms = Room::all();
    	return view('rooms.index')
		    ->with('rooms', $rooms);
    }
}
