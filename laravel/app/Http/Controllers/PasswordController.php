<?php

namespace App\Http\Controllers;

use App\Mailers\ForgetPasswordMailer;
use App\User;
use Carbon\Carbon;
use DB;
use Hash;
use Validator;

class PasswordController extends Controller{

	public function remind(){
		return view('cpanel.users.remind');
	}

	public function generateToken($email){
		$appKey = config('app.key');
		//str_shuffle — Randomly shuffles a string
		//spl_object_hash — Return hash id for given object
		$value = str_shuffle(sha1($email.microtime(true).spl_object_hash($this)));
		$token = hash_hmac('sha256', $value, $appKey);

		$userRemind = DB::table('password_resets')->where('email', $email)->get()->first();
		$reminders = [
			'email'=>$email,
			'token'=>$token,
			'created_at'=>Carbon::now()->toDateTimeString(),
		];

		if($userRemind == null){
			DB::table('password_resets')->insert($reminders);
		}else{
			DB::table('password_resets')->where('email', $email)->update($reminders);
		}

		return $token;
	}

	public function sendRemind(){
		// verify the rules
		$rules = ['email'=>'required|email'];
		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		// check if user exists
		$email = trim(request('email'));
		$user = User::where('email', $email)->get()->first();
		if($user == null){
			return back()->with('errorMessage', 'Email not found');
		}

		$token = $this->generateToken($email);

		if((new ForgetPasswordMailer())->mailForgetPassword($email, $token)){
			return back()->with('successMessage', 'Mail has been sent');
		}

		return back()->with('errorMessage', 'Mail has not been sent. Try later');
	}

	public function change($token){
		return view('cpanel.users.reset')->with('token', $token);
	}

	public function applyChange(){
		$password = trim(request('password'));
		$token = trim(request('token'));
		$email = trim(request('email'));

		$rules = ['password'=>'required'];
		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		// check email exists
		$user = User::where('email', $email)->get()->first();
		if($user == null){
			return back()->with('errorMessage', 'User not found');
		}

		// check if token is correct or expiration date
		$query = DB::table('password_resets')->where('token', $token)->where('email', $email);
		$query->where('created_at', '>', Carbon::now()->subMinutes(40));
		$userByToken = $query->get()->first();
		if($userByToken == null){
			return redirect()->route('password.remind')->with('errorMessage', 'Token is not correct or has expired');
		}

		// update password
		$user->password = Hash::make($password);
		$user->save();

		// clear token
		$query->delete();

		return redirect()->route('login')->with('successMessage', 'User has ben updated');

	}

}
