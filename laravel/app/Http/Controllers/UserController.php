<?php

namespace App\Http\Controllers;

use App\Client;
use App\FailedLogin;
use App\LogUserAction;
use App\Mailers\ForgetPasswordMailer;
use App\Role;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Validator;

class UserController extends Controller{

	public function index(){
		$col = 'id';
		$order = 'desc';
		// filter by order
		if(request()->filled('col') && request()->filled('order')){
			$col = request('col');
			$order = request('order');
		}

		$query = User::orderBy($col, $order);
		$page = request()->has('perPage') ? request('perPage') : 5;
		if(request()->filled('search')){
			$replaceId = str_replace('#', '', request('search'));
			$query->where('id', '=', $replaceId);
			$query->orWhere('email', 'LIKE', '%' . request('search') . '%');
			$query->orWhere('name', 'LIKE', '%' . request('search') . '%');
		}

		// filter by boolean
		if(request('system') == 1) $query->where('system', '=', 1);
		if(request('partner') == 1) $query->where('system', '=', 0);

		$users = $query->paginate($page);
		return view('cpanel.users.index')->with('users', $users);
	}

	public function create(){
		return view('cpanel.users.create');
	}

	public function store(){
		$rules = [
			'email'=>'required|unique:users,email',
			'name'=>'required',
			'password'=>['required'],
		];
		$messages = array();
		$rating = request('getRating');

		$limitValues = [5, 6, 7, 8];
		foreach ($limitValues as $limitValue){
			if($rating < $limitValue){
				$rules['password'][] = 'url';
				$messages = [
					'password.url' => 'Parola este prea slaba, trebuie sa fie mai mare de '.$limitValue,
				];
				break;
			}
		}

		$validator = Validator::make(request()->all(), $rules, $messages);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		$user = new User();
		$user->email = request('email');
		$user->name = request('name');
		$user->password = Hash::make(request('password'));
		$user->save();

		return redirect()->route('users.index')->with('successMessage', 'User has been created');
	}

	public function edit($id){
		$user = User::with('roles')->findOrFail($id);
		$roles = Role::all();
		return view('cpanel.users.edit')
			->with('user', $user)
			->with('roles', $roles);
	}

	public function profile($id){
		$user = User::with('roles')->findOrFail($id);
		return view('cpanel.users.profile')
			->with('user', $user);
	}

	public function update($id){
		$user = User::where('id', $id)->get()->first();
		if($user == null){
			return back()->with('errorMessage', 'User not found');
		}

		$rules = [
			'email'=> ['required'],
			'name'=>'required',
			'password'=>['required'],
		];

		if($user->email != request('email')){
			$rules['email'][] = 'unique:users,email';
		}

		$messages = array();
		$rating = request('getRating');
		//dd($rules);
		$limitValues = [5, 6, 7, 8];
		foreach ($limitValues as $limitValue){
			if($rating < $limitValue){
				$rules['password'][] = 'url';
				$messages = [
					'password.url' => 'Parola este prea slaba, trebuie sa fie mai mare de '.$limitValue,
				];
				break;
			}
		}

		$validator = Validator::make(request()->all(), $rules, $messages);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		DB::beginTransaction();

		$user->email = request('email');
		$user->name = request('name');
		$user->password = Hash::make(request('password'));
		$user->save();

		DB::commit();

		return back()->with('successMessage', 'User has been updated');
	}

	public function destroy(){
		$userIds = request('userIds');
		if($userIds == null){
			return back()->with('errorMessage', 'Please select fields');
		}

		User::destroy($userIds);

		return back()->with('successMessage', 'Fields has been deleted');
	}

	public function login(){

		$countFailedLogins = FailedLogin::failedLogins(20, 0);
		// Set up rechaptcha when the view is loaded method get
		$setRecaptcha = false;
		$admissabelReaptcha = 3;
		if($countFailedLogins >= $admissabelReaptcha){
			$setRecaptcha = true;
		}

		if(request()->isMethod('post')){
			// Validation Rules
			$rules  = [
				'email'=>'required',
				'password'=>'required',
			];
			// add rechaptcha
			$setRecaptcha == true ? $rules['g-recaptcha-response'] = ['required'] : false;
			$validator = Validator::make(request()->all(), $rules);
			if($validator->fails()){
				return back()->withInput()->withErrors($validator);
			}

			// Sleep
			$sleep = 1;
			$admissableFailedLogins = range(3, 99, 3);
			foreach ($admissableFailedLogins as $admissableFailedLogin){
				if($countFailedLogins >= $admissableFailedLogin) {
					$sleep = $sleep + 2;
				} else{
					break;
				}
			}
			sleep($sleep);

			// credentials
			$credentilas['email'] = trim(request('email'));
			$credentilas['password'] = trim(request('password'));

			// check credentials
			if( ! Auth::attempt($credentilas)){
				// save failed logins
				$failedLogin = new FailedLogin();
				$failedLogin->email = $credentilas['email'];
				$failedLogin->ip = \Request::ip();
				$failedLogin->save();

				return redirect()->route('login')->with('errorMessage', 'User or password are incorrect');
			}

			// after login check if the user is still available
			$user = User::where('email', $credentilas['email'])->get()->first();
			if($user == null){
				return redirect()->route('login')->with('errorMessage', 'User not found. Please contact the administrator');
			}

			// register the action for the user
			LogUserAction::storeAction('login', ['Login fara Otp', 'Email' =>$credentilas['email']]);

			return redirect()->intended('blackbox/');
		}
		return view('cpanel.users.login')->with('setRecaptcha', $setRecaptcha);
	}

	public function logout(){
		Auth::logout();
		return redirect()->route('login');
	}

	public function attachRoles($id){
		$user = User::where('id', $id)->get()->first();
		if($user == null){
			return back()->with('errorMessage', 'User not found');
		}

		//Attach Role
		$userRolesIds = request('userRoles');

		$user->roles()->detach();
		$user->roles()->attach($userRolesIds);

		return back()->with('successMessage', 'User has been updated');
	}

	public function impersonate($id){
		User::setImpersonating(Auth::user()->id);
		Auth::loginUsingId($id);
		return back();
	}

	public function stopImpersonate($id){
		User::stopImpersonating();
		Auth::loginUsingId($id);
		return back();
	}

}
