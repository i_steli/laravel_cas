<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Validator;

class ClientController extends Controller
{

    public function index(){
		$perPage = request()->filled('perPage') ? request('perPage') : 3;
		$query = Client::orderBy('id', 'desc');
		if(request()->has('search')){
			$strReplace = str_replace('#', '', request('search'));
			$query->where('id', '=', $strReplace);
			$query->orWhere('first_name', 'LIKE', '%'. request('first_name') .'%');
			$query->orWhere('last_name', 'LIKE', '%'. request('last_name') .'%');
			$query->orWhere('email', 'LIKE', '%'. request('email') .'%');
			$query->orWhere('country', 'LIKE', '%'. request('country') .'%');
		}

	    $clients = $query->paginate($perPage);
        return view('cpanel.clients.index')
	        ->with('clients', $clients);
    }

    public function create(){
		return view('cpanel.clients.create');
    }


    public function store($route){
		$rules = [
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email',
			'phone' => 'required|numeric',
			'country' => 'required',
		];

		//dd($rules, request()->all());

		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}

		$client = new Client();
		$client->first_name = request('first_name');
	    $client->last_name = request('last_name');
	    $client->email = request('email');
	    $client->phone = request('phone');
	    $client->country = request('country');
	    $client->address = request('address');
	    $client->city = request('city');
	    $client->postal_code = request('postal_code');
	    $client->save();

	    return redirect()->route($route)->with('successMessage', 'Client has been added');
    }


    public function show(){

    }

    public function edit($id){
		$client = Client::findOrFail($id);
		return view('cpanel.clients.edit')
			->with('client', $client);
    }


    public function update($id){
		$client = Client::where('id', $id)->get()->first();
		if($client == null){
			return back()->with('errorMessage', 'Client not found');
		}

	    $rules = [
		    'first_name' => 'required',
		    'last_name' => 'required',
		    'email' => 'required|email',
		    'phone' => 'required|numeric',
		    'country' => 'required',
	    ];

	    $validator = Validator::make(request()->all(), $rules);
	    if($validator->fails()){
		    return back()->withInput()->withErrors($validator);
	    }

	    $client->first_name = request('first_name');
	    $client->last_name = request('last_name');
	    $client->email = request('email');
	    $client->phone = request('phone');
	    $client->country = request('country');
	    $client->address = request('address');
	    $client->city = request('city');
	    $client->postal_code = request('postal_code');
	    $client->save();

	    return back()->with('successMessage', 'Client has been added');
    }


    public function destroy(){
		$clientIds = request('clientIds');
		if($clientIds == null){
			return back()->with('errorMessage', 'Please select a field');
		}

	    Client::whereIn('id', $clientIds)->delete();

	    return back()->with('successMessage', 'Field has been deleted');
    }
}
