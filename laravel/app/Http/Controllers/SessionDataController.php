<?php

namespace App\Http\Controllers;

use App\SessionData;

class SessionDataController extends Controller{
	public function index() {
		$perPage = request()->filled( 'perPage' ) ? request( 'perPage' ) : 5;
		$query   = SessionData::orderBy( 'id', 'desc' );
		if ( request()->filled( 'search' ) ) {
			$stringReplace = str_replace( '#', '', request( 'search' ) );
			$query->where( 'id', $stringReplace );
			$query->orWhere( 'action', 'LIKE', '%' . request( 'search' ) . '%' );
		}

		$sessions = $query->paginate($perPage);
		return view('cpanel.sessions.index')->with('sessions', $sessions);
	}

	public function destroy(){
		$sessionsIds = request('sessionsIds');
		if($sessionsIds == null){
			return back()->with('errorMessage', 'Please select a field');
		}

		SessionData::whereIn('id', $sessionsIds)->delete();
		return back()->with('successMessage', 'Sessions were deleted');
	}
}
