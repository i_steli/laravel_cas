<?php

namespace App\Http\Controllers;

use App\Image;
use Exception;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ImageController extends Controller{
	public function index(){
		$perPage = request()->filled('perPage') ? request('perPage') : 3;
		$query = Image::orderBy('id', 'desc');

		if(request()->filled('search')){
			$searchId = str_replace('#', 'LIKE', request('search'));
			$query
				->where('id', '=', $searchId)
				->orWhere('name', 'LIKE', '%' . request('search') . '%');
		}

		$images = $query->paginate($perPage);
		return view('cpanel.images.index')->with('images', $images);
		//return view('cpanel.rooms.index');
	}

	public function store(){
		$rules = [
			'images' => 'required',
			'images.*.room' => 'image|mimes:jpeg,png,jpg',
		];
		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withErrors($validator);
		}

		$basePath = base_path('../public_html');
		$directoryPath = $basePath . '/photos';
		if( ! is_dir($directoryPath)){
			mkdir($directoryPath);
		}

		$images = request('images');
		$fileExists = '';
		foreach ($images as $file){
			$filename = str_replace(' ', '-', $file['room']->getClientOriginalName());
			$filePath = $directoryPath.'/'.$filename;

			if(file_exists($filePath)){
				$fileExists .= $filename;
				continue;
			}

			$url = '/photos'.'/'.$filename;
			$file['room']->move($directoryPath, $filename);
			list($width, $height) = getimagesize($filePath);

			$image = new Image();
			$image->name = $filename;
			$image->url = $url;
			$image->width = $width;
			$image->height = $height;
			$image->file_size = $file['room']->getClientSize();
			$image->file_extension = $file['room']->getClientOriginalExtension();
			$image->save();
		}

		if($fileExists != ''){
			return back()->with('errorMessage', 'Existing files:'.$fileExists);
		}

		return back()->with('successMessage', 'File has been uploaded');
	}


	public function edit($id){
		try{
			$image = Image::findOrFail($id);
		}catch(ModelNotFoundException  $e){
			return back()->with('errorMessage', 'Image not found');
		}

		return view('cpanel.images.edit')->with('image', $image);
	}

	public function update($id){
		$image = Image::where('id', $id)->get()->first();
		if($image == null){
			return back()->with('errorMessage', 'Image not found :)');
		}

		$rules = [
			'name' => 'required',
		];
		$validator = Validator::make(request()->all(), $rules);
		if($validator->fails()){
			return back()->withErrors($validator);
		}

		$directoryPath = '/photos';
		$filePathOld = base_path('../public_html').$image->url;
		$filePathNew = base_path('../public_html').$directoryPath.'/'.request('name');
		//dd($filePath, $fileNew);
		// verify if the file exsits
		if( ! is_readable($filePathOld)){
			return back()->with('errorMessage', 'File not found in server');
		}

		rename($filePathOld, $filePathNew);

		$image->name = request('name');
		$image->url = $directoryPath.'/'.request('name');
		$image->save();

		return back()->with('successMessage', 'File has been updated');
	}

	public function destroy(){
		$deleteImages = request('imageIds');
		if($deleteImages == null){
			return back()->with('errorMessage', 'Please select image');
		}

		$basePath = base_path('../public_html');
		$directoryPath = $basePath.'/photos';

		foreach ($deleteImages as $deleteImage){
			$image = Image::where('id', $deleteImage)->get()->first();
			$filePath = $directoryPath.'/'.$image->name;
			//dd($filePath);
			if(file_exists($filePath)){
				unlink($filePath);
			}
			$image->delete();
		}
		return back()->with('successMessage', 'Image has been deleted');
	}

	public function download($id){
		$image = Image::where('id', $id)->get()->first();
		if($image == null){
			return back()->with('errorMessage', 'Image not found');
		}

		$basePath = base_path('../public_html');
		$directoryPath = $basePath.'/photos';
		$fileName = $image->name;
		$filePath = $directoryPath.'/'.$fileName;
		if(!file_exists($filePath)){
			return back()->with('errorMessage', 'Image on server not found');
		}

		return response()->download($filePath);
	}
}
