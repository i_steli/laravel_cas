<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends \Eloquent{
	// relationships
	public function rooms(){
		return $this->belongsToMany(Room::class);
	}

	public function images(){
		return $this->belongsToMany(Image::class);
	}

	public function hasImage($imageId){
		$galleryHasImage = $this->images()->where('image_id', $imageId)->get()->first();
		if($galleryHasImage != null){
			return true;
		}

		return false;
	}
}
