<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends \Eloquent{
//	relatinships
	public function galleries(){
		return $this->belongsToMany(Gallery::class);
	}
//	functions
	public function resize($size = 720){
		return $this->width < $size ? $this->width : $size;
	}
}
