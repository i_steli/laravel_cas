<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------
| Home:
|--------------------------------------------------------------------------
*/
Route::get('/', array('uses'=>'HomeController@index', 'as'=>'home.index'));
Route::get('/rooms', array('uses'=>'HomeController@rooms', 'as'=>'home.rooms'));



Route::group(['prefix'=>'blackbox'], function(){

	Route::post('/login', array('uses'=>'UserController@login', 'as'=>'login'));
	Route::get('/login', array('uses'=>'UserController@login', 'as'=>'login'));

	Route::get('/password/remind', array('uses'=>'PasswordController@remind', 'as'=>'password.remind'));
	Route::post('/password/send-remind', array('uses'=>'PasswordController@sendRemind', 'as'=>'password.sendRemind'));
	Route::get('/password/change/{token}', array('uses'=>'PasswordController@change', 'as'=>'password.change'));
	Route::post('/password/apply-change', array('uses'=>'PasswordController@applyChange', 'as'=>'password.applyChange'));


	Route::group(['middleware'=>'auth'], function(){
		/*
		|--------------------------------------------------------------------------
		| Welcome:
		|--------------------------------------------------------------------------
		*/
		Route::get('/', array('uses'=>'DashboardController@index', 'as'=>'dashboard.index'));

		Route::get('/logout', array('uses'=>'UserController@logout', 'as'=>'logout'));
		Route::get('/users/stop-impersonate/{id}', array('uses'=>'UserController@stopImpersonate', 'as'=>'users.stopImpersonate'));
	});

		/*
		|--------------------------------------------------------------------------
		| Rooms:
		|--------------------------------------------------------------------------
		*/
		Route::get('/rooms', array('uses'=>'RoomController@index', 'as'=>'rooms.index'))->middleware('permission:roomsIndex');
		Route::get('/rooms/create', array('uses'=>'RoomController@create', 'as'=>'rooms.create'))->middleware('permission:roomsCreate');
		Route::post('/rooms/store', array('uses'=>'RoomController@store', 'as'=>'rooms.store'))->middleware('permission:roomsStore');
		Route::get('/rooms/{id}/edit', array('uses'=>'RoomController@edit', 'as'=>'rooms.edit'))->middleware('permission:roomsEdit');
		Route::put('/rooms/{id}', array('uses'=>'RoomController@update', 'as'=>'rooms.update'))->middleware('permission:roomsUpdate');
		Route::delete('/rooms/destroy', array('uses'=>'RoomController@destroy', 'as'=>'rooms.destroy'))->middleware('permission:roomsDestroy');
		Route::post('/rooms/attach-gallery', array('uses'=>'RoomController@addGallery', 'as'=>'rooms.addGallery'))->middleware('permission:roomsAddGallery');

		/*
		|--------------------------------------------------------------------------
		| Galleries:
		|--------------------------------------------------------------------------
		*/
		Route::get('/galleries', array('uses'=>'GalleryController@index', 'as'=>'galleries.index'))->middleware('permission:galleriesIndex');
		Route::post('/galleries/store', array('uses'=>'GalleryController@store', 'as'=>'galleries.store'))->middleware('permission:galleriesStore');
		Route::get('/galleries/{id}/edit', array('uses'=>'GalleryController@edit', 'as'=>'galleries.edit'))->middleware('permission:galleriesEdit');
		Route::put('/galleries/{id}', array('uses'=>'GalleryController@update', 'as'=>'galleries.update'))->middleware('permission:galleriesUpdate');
		Route::delete('/galleries/destroy', array('uses'=>'GalleryController@destroy', 'as'=>'galleries.destroy'))->middleware('permission:galleriesDestroy');
		Route::post('/galleries/{id}/attach-image', array('uses'=>'GalleryController@addImage', 'as'=>'galleries.addImage'))->middleware('permission:galleriesAddImage');
		Route::post('/galleries/{id}/detach-image', array('uses'=>'GalleryController@removeImage', 'as'=>'galleries.removeImage'))->middleware('permission:galleriesRemoveImage');
		/*
		|--------------------------------------------------------------------------
		| Images:
		|--------------------------------------------------------------------------
		*/
		Route::get('/images', array('uses'=>'ImageController@index', 'as'=>'images.index'))->middleware('permission:imagesIndex');
		Route::post('/images/store', array('uses'=>'ImageController@store', 'as'=>'images.store'))->middleware('permission:imagesStore');
		Route::get('/images/{id}/edit', array('uses'=>'ImageController@edit', 'as'=>'images.edit'))->middleware('permission:imagesEdit');
		Route::get('/images/{id}/download', array('uses'=>'ImageController@download', 'as'=>'images.download'))->middleware('permission:imagesDownload');
		Route::put('/images/{id}', array('uses'=>'ImageController@update', 'as'=>'images.update'))->middleware('permission:imagesUpdate');
		Route::delete('/images/destroy', array('uses'=>'ImageController@destroy', 'as'=>'images.destroy'))->middleware('permission:imagesDestroy');
		/*
		|--------------------------------------------------------------------------
		| Clients:
		|--------------------------------------------------------------------------
		*/
		Route::get('/clients', array('uses'=>'ClientController@index', 'as'=>'clients.index'));
		Route::get('/clients/create', array('uses'=>'ClientController@create', 'as'=>'clients.create'));
		Route::post('/clients/store/{route}', array('uses'=>'ClientController@store', 'as'=>'clients.store'));
		Route::get('/clients/{id}/edit', array('uses'=>'ClientController@edit', 'as'=>'clients.edit'));
		Route::put('/clients/{id}', array('uses'=>'ClientController@update', 'as'=>'clients.update'));
		Route::delete('/clients/destroy', array('uses'=>'ClientController@destroy', 'as'=>'clients.destroy'));
		/*
		|--------------------------------------------------------------------------
		| ReservedRooms:
		|--------------------------------------------------------------------------
		*/
		Route::get('/reserved-rooms', array('uses'=>'ReservedRoomController@index', 'as'=>'reservedRooms.index'));
		Route::get('/reserved-rooms/create', array('uses'=>'ReservedRoomController@create', 'as'=>'reservedRooms.create'));
		Route::post('/reserved-rooms/store', array('uses'=>'ReservedRoomController@store', 'as'=>'reservedRooms.store'));
		Route::get('/reserved-rooms/{id}/edit', array('uses'=>'ReservedRoomController@edit', 'as'=>'reservedRooms.edit'));
		Route::put('/reserved-rooms/{id}', array('uses'=>'ReservedRoomController@update', 'as'=>'reservedRooms.update'));
		Route::delete('/reserved-rooms/destroy', array('uses'=>'ReservedRoomController@destroy', 'as'=>'reservedRooms.destroy'));
		Route::get('/reserved-rooms/{id}/confirmation', array('uses'=>'ReservedRoomController@confirmation', 'as'=>'reservedRooms.confirmation'));
		Route::post('/reserved-rooms/store-client', array('uses'=>'ReservedRoomController@storeClient', 'as'=>'reservedRooms.storeClient'));
		//Route::get('/reserved-rooms/disponible-rooms', array('uses'=>'ReservedRoomController@disponibleRooms', 'as'=>'reservedRooms.disponibleRooms'));

		Route::group(['middleware'=>'userCurrent'], function(){
			Route::get('/users/{id}/edit', array('uses'=>'UserController@edit', 'as'=>'users.edit'));
			Route::get('/users/{id}/profile', array('uses'=>'UserController@profile', 'as'=>'users.profile'));
			Route::put('/users/{id}', array('uses'=>'UserController@update', 'as'=>'users.update'));
		});

		Route::group(['middleware'=>'permission:admin'], function(){
			/*
		|--------------------------------------------------------------------------
		| Roles:
		|--------------------------------------------------------------------------
		*/
			Route::get('/roles', array('uses'=>'RoleController@index', 'as'=>'roles.index'));
			Route::get('/roles/create', array('uses'=>'RoleController@create', 'as'=>'roles.create'));
			Route::post('/roles/store', array('uses'=>'RoleController@store', 'as'=>'roles.store'));
			Route::get('/roles/{id}/edit', array('uses'=>'RoleController@edit', 'as'=>'roles.edit'));
			Route::put('/roles/{id}', array('uses'=>'RoleController@update', 'as'=>'roles.update'));
			Route::delete('/roles/destroy', array('uses'=>'RoleController@destroy', 'as'=>'roles.destroy'));
			Route::post('/roles/attach-permissions/{id}', array('uses'=>'RoleController@attachPermissions', 'as'=>'roles.attachPermissions'));
			/*
			|--------------------------------------------------------------------------
			| Permissions:
			|--------------------------------------------------------------------------
			*/
			Route::get('/permissions', array('uses'=>'PermissionController@index', 'as'=>'permissions.index'));
			Route::get('/permissions/create', array('uses'=>'PermissionController@create', 'as'=>'permissions.create'));
			Route::post('/permissions/store', array('uses'=>'PermissionController@store', 'as'=>'permissions.store'));
			Route::get('/permissions/{id}/edit', array('uses'=>'PermissionController@edit', 'as'=>'permissions.edit'));
			Route::put('/permissions/{id}', array('uses'=>'PermissionController@update', 'as'=>'permissions.update'));
			Route::delete('/permissions/destroy', array('uses'=>'PermissionController@destroy', 'as'=>'permissions.destroy'));
			/*
			|--------------------------------------------------------------------------
			| Users:
			|--------------------------------------------------------------------------
			*/
			Route::get('/users', array('uses'=>'UserController@index', 'as'=>'users.index'));
			Route::get('/users/create', array('uses'=>'UserController@create', 'as'=>'users.create'));
			Route::post('/users/store', array('uses'=>'UserController@store', 'as'=>'users.store'));
			Route::delete('/users/destroy', array('uses'=>'UserController@destroy', 'as'=>'users.destroy'));
			Route::post('/users/attach-roles/{id}', array('uses'=>'UserController@attachRoles', 'as'=>'users.attachRoles'));
			Route::get('/users/impersonate/{id}', array('uses'=>'UserController@impersonate', 'as'=>'users.impersonate'));
			/*
			|--------------------------------------------------------------------------
			| Logs:
			|--------------------------------------------------------------------------
			*/
			Route::get('/logs', array('uses'=>'LogUserActionController@index', 'as'=>'logs.index'));
			Route::delete('/logs/destroy', array('uses'=>'LogUserActionController@destroy', 'as'=>'logs.destroy'));
			/*
			|--------------------------------------------------------------------------
			| Sessions:
			|--------------------------------------------------------------------------
			*/
			Route::get('/sessions', array('uses'=>'SessionDataController@index', 'as'=>'sessions.index'));
			Route::delete('/sessions/destroy', array('uses'=>'SessionDataController@destroy', 'as'=>'sessions.destroy'));
		});
});