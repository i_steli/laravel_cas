<div class="wrapper">

	<div class="header">
		<div class="row">
			<div class="col-sm-6">
				<p class="address">Address line 1, Address line 2</p>
				<h4 class="phone">+40 744 44 33 22</h4>
			</div>
			<div class="col-sm-6 text-right p-0">
				<p class="icon-information">
					<img src="{{ asset('images/others/facebook.svg') }}" class="icon-facebook mr-20">
					<img src="{{ asset('images/others/instagram.svg') }}" class="icon-instagram">
				</p>
			</div>
			<div style="clear:both; height: 60px;"></div>
		</div>

		{{--<div class="row bg-white section">--}}
			{{--<div class="col-sm-8"></div>--}}
			{{--<div class="col-sm-4 text-right">--}}
				{{--<div class="blog-menu animate">--}}
					{{--<div class="btn-blog">--}}
						{{--<p class="blog-title">Meniu</p>--}}
						{{--<span class="blog-bars">--}}
							{{--<span class="bar1 animate"></span>--}}
							{{--<span class="bar2 animate"></span>--}}
							{{--<span class="bar3 animate"></span>--}}
						{{--</span>--}}
					{{--</div>--}}

					{{--<div class="menu-wrapper text-left change animate-5">--}}
						{{--<ul>--}}
							{{--<li>--}}
								{{--<a href="">Ultimele Articole</a>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<a  href="">Multimedia</a>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<a  href="">How To</a>--}}
							{{--</li>--}}
						{{--</ul>--}}
					{{--</div>--}}

				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}

		<div style="clear:both; height: 60px;"></div>

		<div class="row availability">
			<form action="">
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" name="date" id="date" placeholder="YYYY-MM-DD" class="form-control picker-ymd date">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="submit" value="Vezi disponibilitate" class="btn btn-orange text-uppercase btn-block">
					</div>
				</div>
			</form>
		</div>

	</div>
</div>