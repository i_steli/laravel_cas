<div class="sidebar opened">
	<div class="sidebar-close">
		<span class="glyphicon glyphicon-remove"></span>
	</div>
	<div class="sidebar-open">
		<span class="glyphicon glyphicon-align-justify"></span>
		<div class="text-uppercase">Meniu</div>
	</div>

	<div class="sidebar-content">

		<div class="sidebar-logo">
			<a href="{{ route('home.index') }}"><img src="{{ asset('images/logo-castelnor.png') }}"></a>
		</div>

		<ul class="nav nav-sidebar text-uppercase">
			<li class="">
				<a href="">Despre Noi</a>
			</li>
			<li class="">
				<a href="{{ route('home.rooms') }}">Camere</a>
			</li>
			<li>
				<a href="">Hotel</a>
			</li>
			<li>
				<a href="">Bungalows</a>
			</li>
			<li>
				<a href="">Gastronomie</a>
			</li>
			<li>
				<a href="">Spa</a>
			</li>
			<li>
				<a href="">Evenimente</a>
			</li>
			<li>
				<a href="">Contact</a>
			</li>

			<li class="divider"></li>
		</ul>
	</div>
	<div style="clear:both;"></div>
</div>