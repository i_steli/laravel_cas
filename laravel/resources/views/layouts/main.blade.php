<?php $version = '1.200'; ?>
		<!DOCTYPE html>
<html lang="ro">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="index,follow" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon; charset=binary">
	<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon; charset=binary">
	<meta name="google-site-verification" content="ucADjhiMZCODJt432THuCAQ2azYtquAtyvido4Ixrvw" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.1.12.1.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/flatpickr.min.css') }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}?v={{ $version }}">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')" />
</head>
<body class="background header1">

<div class="main">
	@include('includes.header')
	@yield('content')
	@include('includes.footer')
</div>

@include('includes.sidebar')

<div style="clear:both;"></div>


<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.1.12.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/flatpickr.min.js') }}"></script>

<script src="{{ asset('js/custom.js') }}"></script>
{{--<script src="{{ asset('js/jquery.mobile.touch.min.js') }}"></script>--}}
@yield('footer')

</body>
</html>