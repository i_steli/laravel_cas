@extends('layouts.main')

@section('title', 'Homepage')

@section('content')

	<div class="wrapper bg-white-9 mb-20">
		<div class="content-title">
			<h2 class="title">Rooms</h2>
		</div>
		<div class="content-box bg-white">
			@foreach($rooms as $room)
				<div class="row">
					<div class="col-sm-12">
						<h4>{{ $room->name }}</h4>
					</div>
				</div>
				<hr>
			@endforeach
		</div>
	</div>

@stop