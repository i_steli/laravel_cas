@extends('cpanel.layouts.master')

@section('title', 'Reserved Room')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Create Reservation:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('reservedRooms.index') }}">Reservations</a>
				</li>
				<li class="active">
					<strong>Create Reservation</strong>
				</li>
			</ol>
		</div>
	</div>

	<div class="wrapper-content">
		<div class="ibox-content">
			@include('cpanel._partials.message')
			<div class="row">
				<form action="{{ route('reservedRooms.create') }}">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" name="date" id="date" placeholder="YYYY-MM-DD" class="form-control picker-ymd date" value="{{ old('intervalDate') != '' ? old('intervalDate') : request('date') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="submit" value="Vezi disponibilitate" class="btn btn-default text-uppercase btn-block">
						</div>
					</div>
				</form>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<form action="{{ route('reservedRooms.storeClient') }}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="intervalDate" class="intervalDate" value="{{ old('intervalDate') != '' ? old('intervalDate') : request('date')  }}">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<select class="selectpicker form-control" data-live-search="true" title="{{ request()->filled('date') ? 'Rooms disponible between this date:' .request('date') . '(' . count($availableRooms) .')' : 'Rooms disponible this week (' . count($availableRooms) .')' }}" name="availableRoom">

										@foreach($availableRooms as $availableRoom)
											<option value="{{ $availableRoom->id }}" {{ old('availableRoom') == $availableRoom->id ? 'selected' : '' }}>{{ $availableRoom->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<select class="selectpicker form-control" data-live-search="true" title="Select Client" name="client">
										@foreach($clients as $client)
											<option value="{{ $client->id }}" {{ $client->id == old('client') ? 'selected' : '' }}>{{ $client->first_name }} {{ $client->last_name }}</option>
										@endforeach
									</select>
								</div>
							</div>

						</div>
						<div class="form-group">
							<input type="submit" value="Save Client" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	@include('cpanel._partials.create-client', ['btnName' => 'Create Client'])

@stop