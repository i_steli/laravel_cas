@extends('cpanel.layouts.master')

@section('title', 'Reserved Room')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Reserved Rooms</h2>
		</div>
	</div>
	<div class="wrapper-content">

		<div class="ibox-title">
			<a href="{{ route('reservedRooms.create') }}" class="btn btn-primary btn-sm">Create Reserved Room</a>
			<div style="height: 20px;"></div>
			@include('cpanel._partials.message')
		</div>

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($reservedRooms) ? $reservedRooms : ''])

			<div class="table-responsive">
				<form action="{{ route('reservedRooms.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th>Room Name</th>
							<th>View Room</th>
							<th>Client Name</th>
							<th>View Client</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Confirmation</th>
							<th class="text-center">Created at</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						{{--{{ dd($reservedRooms) }}--}}
							@foreach($reservedRooms as $reservedRoom)
							<tr>
								<td>
									<a href="{{ route('reservedRooms.edit', $reservedRoom->id) }}">{{ optional($reservedRoom)->id }}</a>
								</td>
								<td>
									<ul class="list-unstyled">
										@foreach($reservedRoom->rooms as  $room)
											<li>
												<a href="{{ route('rooms.edit', $room->id) }}">{{ optional($room)->name }}</a>
											</li>
										@endforeach
									</ul>
								</td>
								<td>
									<a class="btn btn-sm btn-success">View Room</a>
								</td>
								<td>
									<a>{{ $reservedRoom->getFullName() }}</a>
								</td>
								<td>
									<a class="btn btn-sm btn-success">View Client</a>
								</td>
								<td>
									{{ $reservedRoom->start_date }}
								</td>
								<td>
									{{ $reservedRoom->end_date }}
								</td>
								<td>
									<a href="{{ route('reservedRooms.confirmation', $reservedRoom->id) }}?confirm=1" class="btn btn-sm {{ $reservedRoom->confirmation == 0 ? 'btn-danger' : 'btn-success' }}">{{ $reservedRoom->confirmation == 0 ? 'Not yet' : 'Confirmed' }}</a>
								</td>
								<td class="text-center">{{ optional($reservedRoom)->created_at }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="reservedRoomIds[]" value="{{ $reservedRoom->id }}">
								</td>
							</tr>
							@endforeach
						<tr>
							<td colspan="9"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($reservedRooms) ? $reservedRooms : ''])
		</div>

	</div>

@endsection