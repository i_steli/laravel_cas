<div class="row  border-bottom">
	<nav class="navbar navbar-static-top grey-bg" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header navbar-search">
			<a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i> </a>
			<form class="navbar-form-custom" action="http://nsapp.test/search" style="width:400px">
				<div class="form-group">
					<input type="text" placeholder="Search for something..." class="form-control" name="q" id="q" autocomplete="off">
				</div>

				<div class="search-results hide">
					<h3>Sugestii de cautare:</h3>
					<table class="table table-search-results">

					</table>
					<hr>
					<div class="search-close hide">
						<a>
							<h4>Close</h4>
						</a>
					</div>
				</div>
			</form>
		</div>
		<ul class="nav navbar-top-links navbar-right">
			<li>
				<a href="{{ route('logout') }}">
					<i class="fa fa-sign-out"></i> Log out
				</a>
			</li>
		</ul>
	</nav>
</div>