@inject('help', 'App\Help')
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				@if(Session::has('impersonate'))
					<span  class="block m-t-xs">
						<a href="{{ route('users.stopImpersonate', Session::get('impersonate')) }}"  class="btn btn-success btn-sm">
							Stop impersonating
						</a>
					</span>
				@endif
				<div class="dropdown profile-element">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<span class="clear">
							<span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong> </span>
							<span class="text-muted text-xs block">Utilizator<b class="caret"></b></span>
						</span>
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a href="{{ route('logout') }}">Logout</a></li>
						<li><a href="{{ route('users.profile', Auth::user()->id) }}">Profil</a></li>
					</ul>
				</div>
				<div class="logo-element">
					Castelnor
				</div>
			</li>
			@if(Auth::check() && Auth::user()->hasPermission(['roomsIndex', 'roomsEdit', 'roomsCreate']))
				<li class="{{ $help->hasRoute(['rooms.index', 'rooms.edit', 'rooms.create']) ? 'active' : '' }}">
					<a href="{{ route('rooms.index') }}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Rooms</span> </a>
				</li>
			@endif

			@if(Auth::check() && Auth::user()->hasPermission(['galleriesIndex', 'galleriesEdit', 'galleriesCreate']))
				<li class="{{ $help->hasRoute(['galleries.index','galleries.edit']) ? 'active' : '' }}">
					<a href="{{ route('galleries.index') }}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Galleries</span> </a>
				</li>
			@endif

			@if(Auth::check() && Auth::user()->hasPermission(['imagesIndex', 'imagesEdit', 'imagesCreate']))
				<li class="{{ $help->hasRoute(['images.index', 'images.edit']) ? 'active' : ''}}">
					<a href="{{ route('images.index') }}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Images</span> </a>
				</li>
			@endif

			@if(Auth::check() && Auth::user()->hasPermission(['clientsIndex', 'clientsEdit', 'clientsCreate']))
				<li class="{{ url()->current() == route('clients.index') ? 'active' : '' }}">
					<a href="{{ route('clients.index') }}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Clients</span> </a>
				</li>
			@endif

			@if(Auth::check() && Auth::user()->hasPermission(['reservedRoomsIndex', 'reservedRoomsEdit', 'reservedRoomsCreate']))
			<li class="{{ $help->hasRoute(['reservedRooms.index', 'reservedRooms.create']) ? 'active' : '' }}">
				<a href="{{ route('reservedRooms.index') }}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Reserved Rooms</span> </a>
			</li>
			@endif

			<li class="special_link">
				<a ><i class="fa fa-database"></i> <span class="nav-label">Admin</span></a>
			</li>

			@if(Auth::check() && Auth::user()->hasPermission('admin'))
				<li>
					<div id="wrapper">
						<nav class="navbar-default navbar-static-side" role="navigation">
							<div class="sidebar-collapse">

								<ul class="nav metismenu" id="side-menu">
									<li class="{{ $help->hasRoute(['users.index','roles.index', 'permissions.index', 'users.create', 'roles.create', 'permission.create', 'users.edit', 'roles.edit', 'permission.edit']) ? 'active' : '' }}">
										<a href="#"><i class="fa fa-user"></i> <span class="nav-label">Roluri si permisiuni</span><span class="fa arrow"></span></a>
										<ul class="nav nav-second-level collapse">
											<li class="{{ $help->hasRoute(['users.index', 'users.edit', 'users.create']) ? 'active' : '' }}">
												<a href="{{ route('users.index') }}">Useri</a>
											</li>
											<li class="{{ $help->hasRoute(['roles.index', 'roles.edit', 'roles.create']) ? 'active' : '' }}">
												<a href="{{ route('roles.index') }}">Roluri</a>
											</li>
											<li class="{{ $help->hasRoute(['permissions.index', 'permissions.edit', 'permissions.create']) ? 'active' : '' }}">
												<a href="{{ route('permissions.index') }}">Permisiuni</a>
											</li>
										</ul>
									</li>
								</ul>

								<ul class="nav metismenu" id="side-menu">
									<li class="{{ $help->hasRoute(['logs.index','sessions.index']) ? 'active' : '' }}">
										<a href="#"><i class="fa fa-user"></i> <span class="nav-label">Logs</span><span class="fa arrow"></span></a>
										<ul class="nav nav-second-level collapse">
											<li class="{{ $help->hasRoute('logs.index') ? 'active' : '' }}">
												<a href="{{ route('logs.index') }}">Istoric Utilizatori</a>
											</li>
											<li class="{{ $help->hasRoute('sessions.index') ? 'active' : '' }}">
												<a href="{{ route('sessions.index') }}">Sesiuni</a>
											</li>
										</ul>
									</li>
								</ul>

							</div>
						</nav>
					</div>
				</li>
			@endif

		</ul>
	</div>
</nav>