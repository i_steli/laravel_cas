@inject('help', 'App\Help')
@extends('cpanel.layouts.master')

@section('title', 'Room')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Images</h2>
		</div>
		<div class="col-sm-12">
			<form action="{{ route('images.store') }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="input-group">
					<input type="file" multiple class="form-control" name="images[][room]">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit">Upload</button>
				 	</span>
				</div><!-- /input-group -->
				@include('cpanel._partials.multiple-errors')
			</form>
		</div>
	</div>

	<div class="wrapper-content">

		<div class="ibox-title">
			@include('cpanel._partials.message')
		</div>

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($images) ? $images : ''])

			<div class="table-responsive">
				<form action="{{ route('images.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
						@if(isset($images))
							@foreach($images as $image)
								<div class="cols gapping-sm col-sm-2 image">
									<a href="#" class="thumbnail" data-toggle="modal" data-target="#imageModal{{ $image->id }}">
										<img src="{{ $image->url }}" alt="{{ $image->name }}" style="height: 140px">
									</a>
									<a href="{{ route("images.edit", $image->id) }}" class="edit-btn">
										<span class="glyphicon glyphicon-edit"></span>
									</a>
									<a href="{{ route('images.download', $image->id) }}" class="download-btn">
										<span class="glyphicon glyphicon-download"></span>
									</a>
									<div class="divide-image"></div>
									<div class="delete-btn checkbox">
										<label>
											<input type="checkbox" title="check" name="imageIds[]" value="{{ $image->id }}" class="check-box">
											<span class="glyphicon glyphicon-trash"></span>
										</label>
									</div>
								</div>
								@include('cpanel._partials.modal-images',['record'=>$image])
							@endforeach
						@endif
						<div style="clear: both">
							<div class="col-sm-12">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</div>

						</div>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($images) ? $images : ''])
		</div>

	</div>

@endsection