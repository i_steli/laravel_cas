@inject('help', 'App\Help')
@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Edit Images:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('images.index') }}">Images</a>
				</li>
				<li class="active">
					<strong>Edit Image</strong>
				</li>
			</ol>
		</div>

	</div>

	@if(request()->session()->has('successMessage') || request()->session()->has('errorMessage'))
		<div class="wrapper-content">
			<div class="ibox-content">
				<div class="row">
					<div class="col-sm-12">
						@include('cpanel._partials.message')
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			<div class="row">
				<form action="{{ route('images.update', $image->id) }}" method="post">
					{{ csrf_field() }}
					{{ method_field('put') }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="form-group">
							<label for="name">Name:</label>
							<input type="text" name="name" id="name" value="{{ $image->name }}" placeholder="Name" class="form-control">
							@include('cpanel._partials.first-error', ['field'=>'name'])
						</div>

						<div class="form-group">
							<input type="submit" value="Update" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="wrapper-content">
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-12">
					<h3>Detalii Imagine:</h3>
					<ul class="list-group">
						<li class="list-group-item">
							<span class="badge">{{ $image->url == null ? "File not found" : $image->url  }}</span>
							Path
						</li>
						<li class="list-group-item">
							<span class="badge">{{ $image->width }} px</span>
							Width
						</li>
						<li class="list-group-item">
							<span class="badge">{{ $image->height }} px</span>
							Height
						</li>
						<li class="list-group-item">
							<span class="badge">{{ $help::convertBytes($image->file_size) }}</span>
							File size
						</li>
						<li class="list-group-item">
							<span class="badge">{{ $image->created_at->toFormattedDateString() }}</span>
							Created at
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

@endsection
