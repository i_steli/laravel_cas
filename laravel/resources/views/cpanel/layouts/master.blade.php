<?php  $version = "V2.0" ?>
		<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
	<link href="{{ asset('css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
	<link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/flatpickr.min.css') }}" rel="stylesheet">

	<link href="{{ asset('css/custom.css?') }}version={{ $version }}" rel="stylesheet">
	<title>@yield('title')</title>
</head>

<body>

@hasSection('login')
	@yield('login')
@endif

@hasSection('content')
	<div class="search-overlay"></div>

	<div id="wrapper">

		@include('cpanel.includes.nav')
		<div id="page-wrapper" class="gray-bg">

			@include('cpanel.includes.search-bar')

			@yield('content')

		</div>
	</div>
@endif


<!-- Mainly scripts -->
<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/flatpickr.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('js/inspinia.js') }}"></script>
<script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>

<!-- iCheck -->
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>

<script src="{{ asset('js/custom.js') }}"></script>

<!-- Peity -->
<script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('js/demo/peity-demo.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}" rel="stylesheetfonts"></script>

</body>

</html>