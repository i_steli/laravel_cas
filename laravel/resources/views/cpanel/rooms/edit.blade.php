@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Edit Room:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('rooms.index') }}">Rooms</a>
				</li>
				<li class="active">
					<strong>Edit Room</strong>
				</li>
			</ol>
		</div>
	</div>

	<div class="wrapper-content">
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-12">
					<ul class="list-group">
					<li class="list-group-item">
						<span class="badge">{{ $room->galleries()->count() }}</span>
						Number of galleries selected
					</li>
					</ul>
				</div>
				<div class="col-sm-12">
					<form action="{{ route('rooms.addGallery') }}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="roomId" value="{{ $room->id }}">
						<div class="form-group">
							<select name="roomGalleries[]" class="selectpicker form-control" data-live-search="true"  multiple title="Pick one of the following">
								@foreach($galleries as $gallery)
									<option value="{{ $gallery->id }}" {{ $room->hasGallery($gallery->id) ? 'selected' : '' }}>{{ $gallery->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<input type="submit" value="Add Gallery" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	@if($room->galleries()->count())
	<div class="wrapper-content">
		<div class="ibox-content">
			<div class="row">
				@foreach($room->galleries as $gallery)
					@foreach($gallery->images as $image)
						<div class="cols gapping-sm col-sm-2 image">
							<a href="#" class="thumbnail" data-toggle="modal" data-target="#imageModal{{ $image->id }}">
								<img src="{{ $image->url }}" alt="{{ $image->name }}" style="height: 140px">
							</a>
							<a href="{{ route("images.edit", $image->id) }}" class="edit-btn">
								<span class="glyphicon glyphicon-edit"></span>
							</a>
							<a href="{{ route('images.download', $image->id) }}" class="download-btn">
								<span class="glyphicon glyphicon-download"></span>
							</a>
							<div class="divide-image"></div>
						</div>
						@include('cpanel._partials.modal-images',['record'=>$image])
					@endforeach
				@endforeach
			</div>
		</div>
	</div>
	@endif

	@if(request()->session()->has('successMessage') || request()->session()->has('errorMessage'))
		<div class="wrapper-content">
			<div class="ibox-content">
				<div class="row">
					<div class="col-sm-12">
						@include('cpanel._partials.message')
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			<div class="row">
				<form action="{{ route('rooms.update', $room->id) }}" method="post">
					{{ csrf_field() }}
					{{ method_field('put') }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="form-group">
							<label for="name">Name:</label>
							<input type="text" name="name" id="name" value="{{ $room->name }}" placeholder="Name" class="form-control">
							@include('cpanel._partials.first-error', ['field'=>'name'])
						</div>

						<div class="row">
							<div class="col-sm-5">
								<!-- Adults -->
								<div class="form-group">
									<label for="adults">Number of Adults:</label>
									<select class="form-control adults" name="adults" id="adults">
										<option value="">Select the number</option>
										@foreach(\App\Room::getAdults() as $val)
											<option value="{{ $val }}" {{ $val == $room->adults ? 'selected' : '' }}>{{ $val }}</option>
										@endforeach
									</select>
									@include('cpanel._partials.first-error', ['field'=>'adults'])
								</div>
							</div>
							<div class="col-sm-5">
								<!-- Price -->
								<div class="form-group">
									<label for="initial_price">Initial Price:</label>
									<input type="text" name="initial_price" id="initial_price" value="{{ $room->initial_price }}" placeholder="Price" class="form-control initial_price">
									@include('cpanel._partials.first-error', ['field'=>'initial_price'])
								</div>
							</div>
							<div class="col-sm-2">
								<!-- Currency -->
								<div class="form-group">
									<label for="currency">Currecy:</label>
									<select class="form-control" name="currency" id="currency">
										<option value="">Select currency</option>
										@foreach(\App\Room::getCurrencies() as $key => $val)
											<option value="{{ ucfirst($key) }}" {{ ucfirst($key) == $room->currency ? 'selected' : '' }}>{{ ucfirst($key) }}</option>
										@endforeach
									</select>
									@include('cpanel._partials.first-error', ['field'=>'currency'])
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-5">
								<!-- Adults -->
								<div class="form-group">
									<label for="max-adults">Number of maxim Adults:</label>
									<select class="form-control max-adults" name="max_adults" id="max-adults">
										<option value="">Select the number</option>
										@foreach(\App\Room::getAdults() as $val)
											<option value="{{ $val }}" {{ $val == $room->max_adults ? 'selected' : '' }}>{{ $val }}</option>
										@endforeach
									</select>
									@include('cpanel._partials.first-error', ['field'=>'max_adults'])
								</div>
							</div>
							<div class="col-sm-5">
								<!-- Percentage -->
								<div class="form-group">
									<label for="price_adults">Price Percentage:</label>
									<input type="text" name="price_adults" id="price_adults" value="{{ $room->price_adults  }}" placeholder="%" class="form-control price_adults">
									@include('cpanel._partials.first-error', ['field'=>'price_adults'])
								</div>
							</div>
							<div class="col-sm-2">
								<!-- Calculated Price -->
								<div class="form-group">
									<label >Calculated Price:</label>
									<div class="alert alert-info text-center calculated_price_adult" role="alert" style="padding: 7px">0</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-3">
								<!-- Children -->
								<div class="form-group">
									<label for="children">Number of maxim Children:</label>
									<select class="form-control children" name="children" id="children">
										<option value="">Select the number</option>
										@foreach(\App\Room::getChildren() as $val)
											<option value="{{ $val }}" {{ $val == $room->children ? 'selected' : '' }}>{{ $val }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-sm-2">
								<!-- Percentage -->
								<div class="form-group">
									<label for="set_age">Set maxim age:</label>
									<input type="text" name="set_age" id="set_age" value="{{ $room->set_age }}"  class="form-control set_age">
									@include('cpanel._partials.first-error', ['field'=>'set_age'])
								</div>
							</div>
							<div class="col-sm-5">
								<!-- Percentage -->
								<div class="form-group">
									<label for="price_children">Price Percentage:</label>
									<input type="text" name="price_children" id="price_children" value="{{ $room->price_children }}" placeholder="%" class="form-control price_children">
									@include('cpanel._partials.first-error', ['field'=>'price_children'])
								</div>
							</div>
							<div class="col-sm-2">
								<!-- Calculated Price -->
								<div class="form-group">
									<label >Calculated Price:</label>
									<div class="alert alert-info text-center calculated_price_children" role="alert" style="padding: 7px">0</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<!-- Calculated Price -->
								<label for="promotion">Promotion Percentage:</label>
								<input type="text" name="promotion" id="promotion" value="{{ $room->promotion }}" placeholder="%" class="form-control promotion">
								@include('cpanel._partials.first-error', ['field'=>'promotion'])
							</div>
							<div class="col-sm-6">
								<!-- Calculated Price -->
								<div class="form-group">
									<label >Final Price:</label>
									<div class="alert alert-success text-center final_price" role="alert" style="padding: 7px">0</div>
								</div>
							</div>
						</div>

						<!-- Description -->
						<div class="form-group">
							<label for="description">Description:</label>
							<textarea class="form-control" id="description" name="description" rows="5">{{ $room->description }}</textarea>
						</div>

						<div class="form-group">
							<input type="submit" value="Update" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

@endsection
