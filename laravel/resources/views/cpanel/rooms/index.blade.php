@extends('cpanel.layouts.master')

@section('title', 'Room')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Rooms</h2>
		</div>
	</div>
	<div class="wrapper-content">

		<div class="ibox-title">
			<a href="{{ route('rooms.create') }}" class="btn btn-primary btn-sm">Create Room</a>
			<div style="height: 20px;"></div>
			@include('cpanel._partials.message')
		</div>

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($rooms) ? $rooms : ''])

			<div class="table-responsive">
				<form action="{{ route('rooms.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Gallery</th>
							<th>Price</th>
							<th class="text-center">Currency</th>
							<th class="text-center">Created at</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($rooms))
							@foreach($rooms as $room)
							<tr>
								<td>
									<a href="{{ route('rooms.edit', $room->id) }}">{{ optional($room)->id }}</a>
								</td>
								<td>
									<a href="{{ route('rooms.edit', $room->id) }}">{{ optional($room)->name }}</a>
								</td>
								<td>{{ optional($room)->description }}</td>
								<td>
									<ul class="list-unstyled">
										@foreach($room->galleries as $gallery)
											<li>{{ $gallery->name }}</li>
										@endforeach
									</ul>
								</td>
								<td>
									<a class="btn btn-sm btn-info" data-toggle="collapse" href="#collapse{{$room->id}}" aria-controls="collapse{{$room->id}}">View Prices</a>
								</td>
								<td class="text-center">{{ optional($room)->currency }}</td>
								<td class="text-center">{{ optional($room)->created_at }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="roomIds[]" value="{{ $room->id }}">
								</td>
							</tr>
							<tr>
								<td colspan="8" style="padding: 0;">
									<div class="collapse" id="collapse{{ $room->id }}">
										<table class="table table-bordered">
											<tr>
												<td colspan="3" class="text-center">{{ $room->adults }} Adults</td>
												<td colspan="3" class="text-center">{{ $room->max_adults }} Adults</td>
												<td colspan="2" class="text-center">{{ $room->children }} Children</td>
											</tr>
											<tr>
												<td colspan="3" class="text-center">{{ $room->initial_price }}</td>
												<td colspan="3" class="text-center">{{ $room->getAdultPrice() }}</td>
												<td colspan="2" class="text-center">{{ $room->getChildrenPrice() }}</td>
											</tr>
										</table>
									</div>

								</td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="7"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($rooms) ? $rooms : ''])
		</div>

	</div>

@endsection