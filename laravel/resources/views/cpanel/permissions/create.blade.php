@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Create Permission:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('roles.index') }}">Permissions</a>
				</li>
				<li class="active">
					<strong>Create Permission</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>

	<div style="clear:both; height: 10px;"></div>

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			<div class="row">
				<form action="{{ route('permissions.store') }}" method="post">
					{{ csrf_field() }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="name">Name:</label>
									<input type="text" name="name" id="name" value="{{ request()->old('name') }}" placeholder="Name" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'name'])
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="display_name">Display Name:</label>
									<input type="text" name="display_name" id="display_name" value="{{ request()->old('display_name') }}" placeholder="Display Name" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'display_name'])
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<!-- Description -->
								<div class="form-group">
									<label for="description">Description</label>
									<input type="text" name="description" id="description" value="{{ request()->old('description') }}" placeholder="Description" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="submit" value="Create" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

@endsection
