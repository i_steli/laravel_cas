@extends('cpanel.layouts.master')

@section('title', 'Permissions')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Permissions</h2>
		</div>
	</div>
	<div class="wrapper-content">

		<div class="ibox-title">
			<a href="{{ route('permissions.create') }}" class="btn btn-primary btn-sm">Create Permisson</a>
			<div style="height: 20px;"></div>
			@include('cpanel._partials.message')
		</div>

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($permissions) ? $permissions : ''])

			<div class="table-responsive">
				<form action="{{ route('permissions.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Display Name</th>
							<th>Description</th>
							<th class="text-center">Created</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($permissions))
							@foreach($permissions as $permission)
							<tr>
								<td>
									<a href="{{ route('permissions.edit', $permission->id) }}">{{ optional($permission)->id }}</a>
								</td>
								<td>
									<a href="{{ route('permissions.edit', $permission->id) }}">{{ optional($permission)->name }}</a>
								</td>
								<td>{{ optional($permission)->description }}</td>
								<td>{{ optional($permission)->display_name }}</td>
								<td class="text-center">{{ optional($permission)->created_at->toFormattedDateString() }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="permissionIds[]" value="{{ $permission->id }}">
								</td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="5"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($permissions) ? $permissions : ''])
		</div>

	</div>

@endsection