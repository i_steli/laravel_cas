@extends('cpanel.layouts.master')

@section('title', 'Sessions')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Sessions</h2>
		</div>
	</div>


	<div class="wrapper-content">

		@include('cpanel._partials.message')

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($sessions) ? $sessions : ''])

			<div class="table-responsive">
				<form action="{{ route('logs.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>User</th>
							<th>Email</th>
							<th>Ip</th>
							<th>User Agent</th>
							<th>Last Activity</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($sessions))
							@foreach($sessions as $session)
							<tr>
								<td>
									{{ optional($session->user)->name }}
								</td>
								<td>
									{{ optional($session->user)->email  }}
								</td>
								<td>{{ optional($session)->ip_address }}</td>
								<td>{{ optional($session)->user_agent }}</td>
								<td>{{ \Carbon\Carbon::createFromTimestamp($session->last_activity) }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="sessionIds[]" value="{{ $session->id }}">
								</td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="5"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($sessions) ? $sessions : ''])
		</div>

	</div>

@endsection