@extends('cpanel.layouts.master')

@section('title', 'Welcome')

@section('content')

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				@include('cpanel._partials.message')
				<div class="text-center m-t-lg">
					<h1>
						Welcome in INSPINIA Static SeedProject
					</h1>
					<small>
						It is an application skeleton for a typical web app. You can use it to quickly bootstrap your webapp projects and dev environment for these projects.
					</small>

				</div>
			</div>
		</div>
	</div>
	<div style="clear:both; height: 30px;"></div>

@stop