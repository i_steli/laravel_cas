@extends('cpanel.layouts.master')

@section('title', 'Logs Users')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Logs Users</h2>
		</div>
	</div>

	<div class="wrapper-content">
		<div class="ibox-content">
			<form action="{{ route('logs.index') }}" method="get">
				<div class="row">
					<div class="col-sm-5">
						<input name="start_date" id="start_date" type="text" title="date" class="datepicker form-control" value="{{ request('start_date') }}">
					</div>
					<div class="col-sm-5">
						<input name="end_date" id="end_date" type="text" title="date" class="datepicker form-control" value="{{ request('end_date') }}">
					</div>
					<div class="col-sm-2">
						<input type="submit" class="btn btn-success btn-block" value="Filter">
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="wrapper-content">

		@include('cpanel._partials.message')

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($logs) ? $logs : ''])

			<div class="table-responsive">
				<form action="{{ route('logs.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Data</th>
							<th>Ora</th>
							<th>User</th>
							<th>Action</th>
							<th>
								Description
								@if(request('json') == 'pretty')
									<a href="{{ route('logs.index', array_merge(request()->except('json', 'ste'))) }}">Inline Json</a>
								@else
									<a href="{{ route('logs.index', array_merge(request()->all(), ['json'=>'pretty', 'ste'=>'man'])) }}">Pretty Json</a>
								@endif
							</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($logs))
							@foreach($logs as $log)
							<tr>
								<td>
									{{ optional($log)->created_at->toFormattedDateString() }}
								</td>
								<td>
									{{ optional($log->created_at)->toTimeString()  }}
								</td>
								<td>{{ optional($log)->user->name }}</td>
								<td>{{ optional($log)->action }}</td>
								<td>
									@if(request('json') == 'pretty')
										<pre>{{ optional($log)->changeJson() }}</pre>
									@else
										<pre>{{ optional($log)->description }}</pre>
									@endif
								</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="logIds[]" value="{{ $log->id }}">
								</td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="5"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($rooms) ? $rooms : ''])
		</div>

	</div>

@endsection