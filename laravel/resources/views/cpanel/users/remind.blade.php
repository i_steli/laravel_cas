@extends('cpanel.layouts.master')

@section('login')

	<body class="gray-bg" style="">

	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<div>
				<h1 class="logo-name text-center">CN</h1>
			</div>
			<h3>Am uitat parola</h3>

			@include('cpanel._partials.message')

			<form action="{{ route('password.sendRemind') }}" method="post">
				{{ csrf_field() }}

				<div class="form-group">
					<input type="email" class="form-control" name="email" value=""  autofocus="" title="Email" placeholder="Email">
					@include('cpanel._partials.first-error', ['field'=>'email'])
				</div>

				<button type="submit" class="btn btn-primary block full-width m-b">Trimite parola resetat</button>

			</form>
		</div>
	</div>

	</body>

@stop