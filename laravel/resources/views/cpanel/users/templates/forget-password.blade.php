<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>test</title>
</head>
<body>
	<h1>Subject: {{ $subject }}</h1>
	<h3>Salut {{ $email }}</h3>
	<p>Pentru a reseta parola, te rog acceseaza linkul:</p>
	<p>
		<a href="{{ route('password.change', $token) }}?email={{ $email }}">Reset</a>
	</p>
</body>
</html>