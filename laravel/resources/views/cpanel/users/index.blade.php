@extends('cpanel.layouts.master')

@section('title', 'Users')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Users</h2>
		</div>
	</div>
	<div class="wrapper-content">

		<div class="ibox-title">
			<div class="row">
				<div class="col-sm-4">
					<a href="{{ route('users.create') }}" class="btn btn-primary btn-sm">Create User</a>
				</div>
				<div class="col-sm-8 text-right stars">
					<a href="{{ route('users.index', \App\User::toggleFilter('system', ['perPage', 'search', 'page'])) }}" class="btn btn-default btn-sm">
						<span class="{{ request('system') == 1 ? 'glyphicon glyphicon-star' : 'glyphicon glyphicon-star-empty' }}"></span> System
					</a>
					<a href="{{ route('users.index', \App\User::toggleFilter('partner', ['perPage', 'search', 'page'])) }}" class="btn btn-default btn-sm">
						<span class="{{ request('partner') == 1 ? 'glyphicon glyphicon-star' : 'glyphicon glyphicon-star-empty' }}"></span> Partner
					</a>
					<a href="{{ route('users.index', \App\User::toggleFilter('all', ['perPage', 'search', 'page'])) }}" class="btn btn-default btn-sm">
						<span class="{{ request('all')  ? 'glyphicon glyphicon-star' : 'glyphicon glyphicon-star-empty' }}"></span> Toate
					</a>
				</div>
			</div>
			<div style="height: 20px;"></div>
			@include('cpanel._partials.message')
		</div>

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($users) ? $users : ''])

			<div class="table-responsive">
				<form action="{{ route('users.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th>
								<a href="{{ route('users.index', \App\User::toggleOrder('name')) }}">
									Name
									@if(request('order') == 'asc' && request('col') == 'name')
										<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
									@elseif(request('order') == 'desc' && request('col') == 'name')
										<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
									@endif
								</a>
							</th>
							<th>Email</th>
							<th>System</th>
							<th class="text-center">
								<a href="{{ route('users.index', \App\User::toggleOrder('created_at')) }}">
									Created
									@if(request('order') == 'asc' &&  request('col') == 'created_at')
										<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
									@elseif(request('order') == 'desc' && request('col') == 'created_at')
										<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
									@endif
								</a>
							</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($users))
							@foreach($users as $user)
							<tr>
								<td>
									<a href="{{ route('users.profile', $user->id) }}">{{ optional($user)->id }}</a>
								</td>
								<td>
									<a href="{{ route('users.profile', $user->id) }}">{{ optional($user)->name }}</a>
								</td>
								<td>{{ optional($user)->email }}</td>
								<td>
									<span class="label {{ $user->system == 1 ? 'label-success' : 'label-danger' }}">{{ optional($user)->system == 1 ? 'Da' : 'Nu' }}</span>
								</td>
								<td class="text-center">{{ optional($user)->created_at->toDateString() }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="userIds[]" value="{{ $user->id }}">
								</td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="5"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($users) ? $users : ''])
		</div>

	</div>

@endsection