@extends('cpanel.layouts.master')

@section('login')

	<body class="gray-bg" style="">

	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<div>
				<h1 class="logo-name text-center">CN</h1>
			</div>
			<h3>Reset Password</h3>

			@include('cpanel._partials.message')

			<form action="{{ route('password.applyChange') }}" method="post">
				{{ csrf_field() }}
				<input type="hidden" name="token" value="{{ $token }}">
				<input type="hidden" name="email" value="{{ request('email') }}">

				<div class="form-group">
					<input type="password" class="form-control" name="password" value=""  autofocus="" title="Email" placeholder="Password">
					@include('cpanel._partials.first-error', ['field'=>'password'])
				</div>

				<button type="submit" class="btn btn-primary block full-width m-b">Modifica Parola</button>

			</form>
		</div>
	</div>

	</body>

@stop