@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Profile User</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('users.index') }}">Users</a>
				</li>
				<li class="active">
					<strong>Profile</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>

	<div style="clear:both; height: 10px;"></div>

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			@include('cpanel._partials.message')
			<div class="row">
				<div class="col-sm-6">
					<h4>Email: {{ $user->email }}</h4>
					<h4>Utilizatorul: {{ $user->name }}</h4>
				</div>
				{{ Session::get('impersonate') }}
				<div class="col-sm-6 text-right">
					<!-- check if the user is admin, the user can't impersonate himself, impersonate a single user -->
					@if(Auth()->user()->hasPermission('admin') && Auth::user()->id != $user->id && !Session::has('impersonate'))
						<h4>
							<a href="{{ route('users.impersonate', $user->id) }}" class="btn btn-success">Impersonate User</a>
						</h4>
					@endif
				</div>
				<div class="col-sm-12">
					<hr>
				</div>
				<div class="col-sm-12">
					<h4>Roluri:</h4>
					<ol type="1">
						@foreach($user->roles as $role)
							<li>{{ $role->display_name }}</li>
						@endforeach
					</ol>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<a href="{{ route('users.edit', $user->id) }}" class="btn btn-success">Editeaza utilizator</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
