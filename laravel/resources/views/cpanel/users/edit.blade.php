@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Update User:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('users.index') }}">Users</a>
				</li>
				<li class="active">
					<strong>Update User</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>

	<div style="clear:both; height: 10px;"></div>

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			@include('cpanel._partials.message')
			<div class="row">
				<form action="{{ route('users.update', $user->id) }}" method="post">
					{{ csrf_field() }}
					{{ method_field('put') }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="first_name">Username:</label>
									<input type="text" name="name" id="name" value="{{ old('name') == null ? $user->name : old('name')  }}" placeholder="Username" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'name'])
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email">Email:</label>
									<input type="text" name="email" id="email" value="{{ old('email') == null ? $user->email : old('email') }}" placeholder="Email" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'email'])
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<!-- Email -->
								<div class="form-group">
									<label for="password">
										Parola:
										<span class="label label-info">Securitate parola: <span class="rating">0/10</span> | Caractere: <span class="strings">0</span></span>
									</label>
									<div class="input-group">
										<input type="password" name="password" id="password" value="{{ request()->old('password') }}" placeholder="Password" class="form-control password">
  										<span class="input-group-btn">
											<button class="btn btn-default show-password" type="button">
												<span class="glyphicon glyphicon-eye-close"></span>
											</button>
										</span>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="col-rating-default relative hide">
												<div class="col-rating"></div>
											</div>
											<input type="hidden" name="getRating" class="getRating">
										</div>
									</div>

									@include('cpanel._partials.first-error', ['field'=>'password'])
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="submit" value="Update" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="wrapper-content">
		<div class="ibox-content">
			<form action="{{ route('users.attachRoles', $user->id) }}" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-sm-12">
						<h4>Number of roles selected: {{ $user->roles()->count() }}</h4>
						<hr>
						@foreach($roles as $role)
							<div class="checkbox">
								<label>
									<input type="checkbox" name="userRoles[]" value="{{ $role->id }}" {{ $user->hasRole($role->id)  }}>
									{{ $role->name }}
								</label>
							</div>
						@endforeach
					</div>
				</div>

				<div class="form-group">
					<input type="submit" value="Update Roles" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>

@endsection
