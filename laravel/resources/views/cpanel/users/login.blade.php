@extends('cpanel.layouts.master')

@section('login')

	<body class="gray-bg" style="">

	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<div>
				<h1 class="logo-name text-center">CN</h1>
			</div>
			<h3>Login in BBOX</h3>

			@include('cpanel._partials.message')

			<form action="{{ route('login') }}" method="post">
				{{ csrf_field() }}

				<div class="form-group">
					<input type="email" class="form-control" name="email" value="" required="" autofocus="" title="Email">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password" required="" title="Password">
				</div>
				@if($setRecaptcha)
					<div class="group">
						{!! Recaptcha::render() !!}
					</div>
					@include('cpanel._partials.first-error', ['field' => 'g-recaptcha-response'])
				@endif
				<button type="submit" class="btn btn-primary block full-width m-b">Login</button>

				<a href="{{ route('password.remind') }}">
					<small>Ai uitat parola?</small>
				</a>
			</form>
		</div>
	</div>

	</body>

@stop