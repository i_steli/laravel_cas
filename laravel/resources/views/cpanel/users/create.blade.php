@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Create User:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('users.index') }}">Users</a>
				</li>
				<li class="active">
					<strong>Create User</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>

	<div style="clear:both; height: 10px;"></div>

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			<div class="row">
				<form action="{{ route('users.store') }}" method="post">
					{{ csrf_field() }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="first_name">Username:</label>
									<input type="text" name="name" id="name" value="{{ request()->old('name') }}" placeholder="Username" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'name'])
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email">Email:</label>
									<input type="text" name="email" id="email" value="{{ request()->old('email') }}" placeholder="Email" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'email'])
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<!-- Email -->
								<div class="form-group">
									<label for="password">
										Parola:
										<span class="label label-info">Securitate parola: <span class="rating">0/10</span> | Caractere: <span class="strings">0</span></span>
									</label>
									<div class="input-group">
										<input type="password" name="password" id="password" value="{{ request()->old('password') }}" placeholder="Password" class="form-control password">
  										<span class="input-group-btn">
											<button class="btn btn-default show-password" type="button">
												<span class="glyphicon glyphicon-eye-close"></span>
											</button>
										</span>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="col-rating-default relative hide">
												<div class="col-rating"></div>
											</div>
											<input type="hidden" name="getRating" class="getRating">
										</div>
									</div>

									@include('cpanel._partials.first-error', ['field'=>'password'])
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="submit" value="Create" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

@endsection
