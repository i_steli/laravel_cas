@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Edit Gallery:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('galleries.index') }}">Galleries</a>
				</li>
				<li class="active">
					<strong>Edit Gallery</strong>
				</li>
			</ol>
		</div>

	</div>

	@if(request()->session()->has('successMessage') || request()->session()->has('errorMessage'))
		<div class="wrapper-content">
			<div class="ibox-content">
				<div class="row">
					<div class="col-sm-12">
						@include('cpanel._partials.message')
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			<div class="row">
				<form action="{{ route('galleries.update', $gallery->id) }}" method="post">
					{{ csrf_field() }}
					{{ method_field('put') }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="form-group">
							<label for="name">Name:</label>
							<input type="text" name="name" id="name" value="{{ $gallery->name }}" placeholder="Name" class="form-control">
							@include('cpanel._partials.first-error', ['field'=>'name'])
						</div>

						<div class="form-group">
							<input type="submit" value="Update" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	@if($gallery->images->count())
		<div class="wrapper-content">
			<div class="ibox-content">
				<form action="{{ route('galleries.removeImage', $gallery->id) }}" method="post">
					{{ csrf_field() }}

					<div class="row">
						@foreach($gallery->images as $image)
							<div class="cols gapping-sm col-sm-2 image">
								<a href="#" class="thumbnail" data-toggle="modal" data-target="#imageModal{{ $image->id }}">
									<img src="{{ $image->url }}" alt="{{ $image->name }}" style="height: 140px">
								</a>
								<a href="{{ route("images.edit", $image->id) }}" class="edit-btn">
									<span class="glyphicon glyphicon-edit"></span>
								</a>
								<a href="{{ route('images.download', $image->id) }}" class="download-btn">
									<span class="glyphicon glyphicon-download"></span>
								</a>
								<div class="divide-image"></div>
								<div class="delete-btn checkbox">
									<label>
										<input type="hidden" name="imageId" value="{{ $image->id }}">
										<button class="glyphicon glyphicon-trash"></button>
									</label>
								</div>
				 			</div>
							@include('cpanel._partials.modal-images',['record'=>$image])
						@endforeach
					</div>

				</form>

			</div>
		</div>
	@endif

	<div class="wrapper-content">
		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($images) ? $images : ''])

			<div class="table-responsive">
				<form action="{{ route('galleries.addImage', $gallery->id) }}" method="post">
					{{ csrf_field() }}

					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th class="text-center">Thumb</th>
							<th class="text-center">Created at</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox">Select all Images</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($images))
							@foreach($images as $image)
								<tr>
									<td>
										<a href="{{ route('images.edit', $image->id) }}">{{ optional($image)->id }}</a>
									</td>
									<td>
										<a href="{{ route('images.edit', $image->id) }}">{{ optional($image)->name }}</a>
									</td>
									<td class="text-center">
										<a data-toggle="modal" data-target="#imageModal{{ $image->id }}">
											<img src="{{ $image->url }}" width="100;">
										</a>
									</td>
									<td class="text-center">{{ $image->created_at }}</td>
									<td class="text-center">
										<input type="checkbox" class="check-box" title="check" name="imageIds[]" value="{{ $image->id }}" {{ $gallery->hasImage($image->id) ? "checked" : ''}}>
									</td>
								</tr>
								@include('cpanel._partials.modal-images',['record'=>$image])
							@endforeach
						@endif
						<tr>
							<td colspan="4"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Toggle Image</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
			@include('cpanel._partials.footer-settings', ['records'=>isset($images) ? $images : ''])
		</div>

	</div>

@endsection
