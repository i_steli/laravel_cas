@extends('cpanel.layouts.master')

@section('title', 'Gallery')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Galleries</h2>
		</div>
	</div>

	<div class="wrapper-content">
		<div class="ibox-content">
			<div class="row">
				<form action="{{ route('galleries.store') }}" method="post">
					{{ csrf_field() }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="form-group">
							<label for="name">Name:</label>
							<input type="text" name="name" id="name" value="" placeholder="Name" class="form-control">
							@include('cpanel._partials.first-error', ['field'=>'name'])
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<input type="submit" value="Create" class="btn btn-success">
						</div>
					</div>
				</form>
				<div class="col-sm-12">
					@include('cpanel._partials.message')
				</div>
			</div>
		</div>
	</div>

	<div class="wrapper-content">

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($galleries) ? $galleries : ''])

			<div class="table-responsive">
				<form action="{{ route('galleries.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th class="text-center">Name</th>
							<th class="text-center">Room Name</th>
							<th class="text-center">Created at</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($galleries))
							@foreach($galleries as $gallery)
							<tr>
								<td>
									<a href="{{ route('galleries.edit', $gallery->id) }}">{{ optional($gallery)->id }}</a>
								</td>
								<td class="text-center">
									<a href="{{ route('galleries.edit', $gallery->id) }}">{{ optional($gallery)->name }}</a>
								</td>
								<td class="text-center"></td>
								<td class="text-center">{{ optional($gallery)->created_at }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="galleryIds[]" value="{{ $gallery->id }}">
								</td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="4"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($galleries) ? $galleries : ''])
		</div>

	</div>

@endsection