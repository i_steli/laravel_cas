@extends('cpanel.layouts.master')

@section('title', 'Roles')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Roles</h2>
		</div>
	</div>
	<div class="wrapper-content">

		<div class="ibox-title">
			<a href="{{ route('roles.create') }}" class="btn btn-primary btn-sm">Create Role</a>
			<div style="height: 20px;"></div>
			@include('cpanel._partials.message')
		</div>

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($roles) ? $roles : ''])

			<div class="table-responsive">
				<form action="{{ route('roles.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Display Name</th>
							<th>Description</th>
							<th class="text-center">Created</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($roles))
							@foreach($roles as $role)
							<tr>
								<td>
									<a href="{{ route('roles.edit', $role->id) }}">{{ optional($role)->id }}</a>
								</td>
								<td>
									<a href="{{ route('roles.edit', $role->id) }}">{{ optional($role)->name }}</a>
								</td>
								<td>{{ optional($role)->description }}</td>
								<td>{{ optional($role)->display_name }}</td>
								<td class="text-center">{{ optional($role)->created_at->toFormattedDateString() }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="roleIds[]" value="{{ $role->id }}">
								</td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="5"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($roles) ? $roles : ''])
		</div>

	</div>

@endsection