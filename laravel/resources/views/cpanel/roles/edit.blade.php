@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Edit Role:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('roles.index') }}">Roles</a>
				</li>
				<li class="active">
					<strong>Edit Role</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>

	<div style="clear:both; height: 10px;"></div>

	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			<div class="row">
				@include('cpanel._partials.message')
				<form action="{{ route('roles.update', $role->id) }}" method="post">
					{{ csrf_field() }}
					{{ method_field('put') }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="name">Name:</label>
									<input type="text" name="name" id="name" value="{{ old('name') == null ? $role->name : old('name')}}" placeholder="Name" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'name'])
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="display_name">Display Name:</label>
									<input type="text" name="display_name" id="display_name" value="{{ old('display_name') == null ? $role->display_name : old('display_name') }}" placeholder="Email" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'display_name'])
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<!-- Description -->
								<div class="form-group">
									<label for="description">Description</label>
									<input type="text" name="description" id="description" value="{{ old('description') == null ? $role->description : old('description')  }}" placeholder="Description" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="submit" value="Update" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="wrapper-content">
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-12">
					<ul class="list-group">
						<li class="list-group-item">
							<span class="badge">{{ $role->permissions()->count() }}</span>
							Number of permissions selected
						</li>
					</ul>
				</div>
				<div class="col-sm-12">
					<form action="{{ route('roles.attachPermissions', $role->id) }}" method="post">
						{{ csrf_field() }}
						<div class="form-group">
							<select name="rolePermissions[]" class="selectpicker form-control" data-live-search="true"  multiple title="Pick one of the following">
								@foreach($permissions as $permission)
									<option value="{{ $permission->id }}" {{ $role->hasPermisson($permission->id) ? 'selected' : '' }}>{{ $permission->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<input type="submit" value="Add Gallery" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection
