@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Edit Client:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('clients.index') }}">Clients</a>
				</li>
				<li class="active">
					<strong>Edit Client</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>

	<div style="clear:both; height: 10px;"></div>


	<div class="wrapper-content">
		<div class="ibox-content border-bottom">
			@include('cpanel._partials.message')
			<div class="row">
				<form action="{{ route('clients.update', $client->id) }}" method="post">
					{{ method_field('put') }}
					{{ csrf_field() }}
					<div class="col-sm-12">
						<!-- Name -->
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="first_name">First Name:</label>
									<input type="text" name="first_name" id="first_name" value="{{ $client->first_name }}" placeholder="First Name" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'first_name'])
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="last_name">Last Name:</label>
									<input type="text" name="last_name" id="last_name" value="{{ $client->last_name }}" placeholder="Last Name" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'last_name'])
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<!-- Email -->
								<div class="form-group">
									<label for="email">Email:</label>
									<input type="text" name="email" id="email" value="{{ $client->email }}" placeholder="Email" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'email'])
								</div>
							</div>
							<div class="col-sm-6">
								<!-- Phone -->
								<div class="form-group">
									<label for="phone">Phone:</label>
									<input type="text" name="phone" id="phone" value="{{ $client->phone }}" placeholder="Phone" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'phone'])
								</div>
							</div>
						</div>

						<!-- Country -->
						<div class="form-group">
							<label for="country">Country:</label>
							<input type="text" name="country" id="country" value="{{ $client->country }}" placeholder="Country" class="form-control">
							@include('cpanel._partials.first-error', ['field'=>'country'])
						</div>


						<div class="form-group">
							<label for="address">Address:</label>
							<textarea name="address" id="address" rows="5" placeholder="Address" class="form-control">{{ $client->address }}</textarea>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<!-- City -->
								<div class="form-group">
									<label for="city">City:</label>
									<input type="text" name="city" id="city"  placeholder="City" value="{{ $client->city }}" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<!-- Phone -->
								<div class="form-group">
									<label for="postal_code">Postal Code:</label>
									<input type="text" name="postal_code" id="postal_code" value="{{ $client->postal_code }}" placeholder="Postal Code" class="form-control">
									@include('cpanel._partials.first-error', ['field'=>'postal_code'])
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="submit" value="Edit" class="btn btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

@endsection
