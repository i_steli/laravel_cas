@extends('cpanel.layouts.master')

@section('title', 'Clinet')

@section('content')

	<!-- Main view  -->
	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-12">
			<h2>Clinets</h2>
		</div>
	</div>
	<div class="wrapper-content">

		<div class="ibox-title">
			<a href="{{ route('clients.create') }}" class="btn btn-primary btn-sm">Create Clinet</a>
			<div style="height: 20px;"></div>
			@include('cpanel._partials.message')
		</div>

		<div class="ibox-content">
			@include('cpanel._partials.header-settings', ['records'=>isset($clients) ? $clients : ''])

			<div class="table-responsive">
				<form action="{{ route('clients.destroy') }}" method="post">
					{{ csrf_field() }}
					{{ method_field('delete') }}
					<table class="table table-striped">
						<thead>
						<tr>
							<th>ID</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Country</th>
							<th>View Address</th>
							<th class="text-center">Created at</th>
							<th class="text-center">
								<label><input type="checkbox" class="toggleCheckbox"> Delete Toggle</label>
							</th>
						</tr>
						</thead>
						<tbody>
						@if(isset($clients))
							@foreach($clients as $client)
							<tr>
								<td>
									<a href="{{ route('clients.edit', $client->id) }}">{{ optional($client)->id }}</a>
								</td>
								<td>
									<a href="{{ route('clients.edit', $client->id) }}">{{ optional($client)->last_name }}</a>
								</td>
								<td>
									<a href="{{ route('clients.edit', $client->id) }}">{{ optional($client)->first_name }}</a>
								</td>
								<td>{{ optional($client)->email }}</td>
								<td>{{ optional($client)->phone }}</td>
								<td>{{ optional($client)->country }}</td>
								<td>
									<a class="btn btn-success btn-xs" data-toggle="collapse" href="#{{ $client->first_name }}" aria-controls="{{ $client->first_name }}">View Address</a>
								</td>

								<td class="text-center">{{ optional($client)->created_at }}</td>
								<td class="text-center">
									<input type="checkbox" class="check-box" title="check" name="clientIds[]" value="{{ $client->id }}">
								</td>
							</tr>
							<tr>
								<td colspan="9" style="padding: 0">
									<div class="collapse" id="{{ $client->first_name }}" aria-expanded="false">
										<table class="table table-bordered">
											<tr>
												<td>Address</td>
												<td>City</td>
												<td>Zip Code</td>
											</tr>
											<tr>
												<td>{{ $client->address }}</td>
												<td>{{ $client->city }}</td>
												<td>{{ $client->postal_code }}</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>

							@endforeach
						@endif
						<tr>
							<td colspan="8"></td>
							<td class="text-center">
								<button class="btn btn-success btn-md" type="submit">Delete</button>
							</td>
						</tr>
						</tbody>
					</table>
				</form>
			</div>
				@include('cpanel._partials.footer-settings', ['records'=>isset($clients) ? $clients : ''])
		</div>

	</div>

@endsection