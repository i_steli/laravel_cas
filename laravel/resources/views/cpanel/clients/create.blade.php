@extends('cpanel.layouts.master')

@section('content')

	<div class="row page-heading white-bg border-bottom">
		<div class="col-lg-10">
			<h2>Create Client:</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ route('clients.index') }}">Clients</a>
				</li>
				<li class="active">
					<strong>Create Client</strong>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">

		</div>
	</div>

	<div style="clear:both; height: 10px;"></div>

	@include('cpanel._partials.create-client')

@endsection
