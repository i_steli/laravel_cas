<div class="modal fade" id="imageModal{{ isset($record) ? $record->id : '' }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog text-center" role="document">
		<img src="{{ isset($record) ? $record->url : ''}}" alt="{{ isset($record) ? $record->name : '' }}"  width="{{ isset($record) ? $record->resize() : '' }}">
	</div>
</div>