@if($errors->first($field) != null)
	<span class="label label-danger">{{ isset($errors) ? $errors->first($field) : '' }}</span>
@endif