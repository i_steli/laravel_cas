<div class="wrapper-content">
	<div class="ibox-content border-bottom">
		<div class="row">
			<form action="{{ route('clients.store', Route::currentRouteName()) }}" method="post">
				{{ csrf_field() }}
				<div class="col-sm-12">
					<!-- Name -->
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="first_name">First Name:</label>
								<input type="text" name="first_name" id="first_name" value="{{ request()->old('first_name') }}" placeholder="First Name" class="form-control">
								@include('cpanel._partials.first-error', ['field'=>'first_name'])
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="last_name">Last Name:</label>
								<input type="text" name="last_name" id="last_name" value="{{ request()->old('last_name') }}" placeholder="Last Name" class="form-control">
								@include('cpanel._partials.first-error', ['field'=>'last_name'])
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<!-- Email -->
							<div class="form-group">
								<label for="email">Email:</label>
								<input type="text" name="email" id="email" value="{{ request()->old('email') }}" placeholder="Email" class="form-control">
								@include('cpanel._partials.first-error', ['field'=>'email'])
							</div>
						</div>
						<div class="col-sm-6">
							<!-- Phone -->
							<div class="form-group">
								<label for="phone">Phone:</label>
								<input type="text" name="phone" id="phone" value="{{ request()->old('phone') }}" placeholder="Phone" class="form-control">
								@include('cpanel._partials.first-error', ['field'=>'phone'])
							</div>
						</div>
					</div>

					<!-- Country -->
					<div class="form-group">
						<label for="country">Country:</label>
						<input type="text" name="country" id="country" value="{{ request()->old('country') }}" placeholder="Country" class="form-control">
						@include('cpanel._partials.first-error', ['field'=>'country'])
					</div>

					<div class="form-group">
						<button type="button" class="btn btn-default add-address" data-toggle="collapse" href="#address">
							Add address optional <span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>

					<div class="collapse" id="address">
						<div class="form-group">
							<label for="address">Address:</label>
							<textarea name="address" id="address" rows="5" placeholder="Address" class="form-control">{{ request()->old('address') }}</textarea>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<!-- City -->
								<div class="form-group">
									<label for="city">City:</label>
									<input type="text" name="city" id="city"  placeholder="City" value="{{ request()->old('city') }}" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<!-- Phone -->
								<div class="form-group">
									<label for="postal_code">Postal Code:</label>
									<input type="text" name="postal_code" id="postal_code" value="{{ request('postal_code') == 0 ? request()->old('postal_code') : 0}}" placeholder="Postal Code" class="form-control">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<input type="submit" value="{{ isset($btnName) ? $btnName : 'Create' }}" class="btn btn-success">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>