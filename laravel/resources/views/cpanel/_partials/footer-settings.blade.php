<div style="clear: both;"></div>
<div class="col-lg-8 col-md-6 col-sm-6">
	{{ $records != '' ? $records->appends(request()->all())->links() : '' }}
</div>
<form action="{{ url()->current() }}">
	@foreach(request()->except('perPage') as $key => $val)
		<input type="hidden" name="{{ $key }}" value="{{ $val }}">
	@endforeach
	<div class="col-lg-4 col-md-6 col-sm-6 text-right">
		<div class="form-group">
			<label for="choose">
				Rezultate pe pagina
			</label>
			<select class="form-control perPage" id="choose" name="perPage" title="choose">
				<option {{ request('perPage') == 3 ? 'selected' : '' }}>3</option>
				<option {{ request('perPage') == 5 ? 'selected' : '' }}>5</option>
				<option {{ request('perPage') == 10 ? 'selected' : '' }}>10</option>
				<option {{ request('perPage') == 15 ? 'selected' : '' }}>15</option>
			</select>
		</div>
	</div>
</form>
<div style="clear: both;"></div>