<div class="col-sm-12">
	@if( is_object($records))
		<h4>{{ $records->total() != 0 ? 'Total:'.$records->total() : ''}}</h4>
	@endif
</div>
<div style="clear: both;"></div>
<div class="col-lg-8 col-md-6 col-sm-6">
	{{ $records != '' ? $records->appends(request()->all())->links() : '' }}
</div>
<form action="{{ url()->current() }}">
	@foreach(request()->except('search') as $key => $val)
		<input type="hidden" name="{{ $key }}" value="{{ $val }}">
	@endforeach
	<div class="col-lg-4 col-md-6 col-sm-6 text-right">
		<div class="input-group" style="padding: 20px 0;">
			<input type="text" class="form-control" placeholder="Search for..." name="search">
			<span class="input-group-btn">
				<button class="btn btn-default"  type="submit">Go!</button>
			</span>
		</div>
	</div>
</form>
<div style="clear: both;"></div>