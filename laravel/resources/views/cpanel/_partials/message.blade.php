@if(request()->session()->has('successMessage'))
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ request()->session()->get('successMessage') }}
	</div>
@endif

@if(request()->session()->has('errorMessage'))
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{{ request()->session()->get('errorMessage') }}
	</div>
@endif