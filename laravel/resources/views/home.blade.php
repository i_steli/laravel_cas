@extends('layouts.main')

@section('title', 'Homepage')

@section('content')

	<div class="wrapper bg-white-9 mb-20">
		<div class="content-title">
			<h2 class="title">Gallery</h2>
		</div>

		<div class="content-box bg-white">
			<div class="row">
				<div class="col-md-12">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp mb-20">
								<div class="carousel-caption">
									Descriere imagine
								</div>
							</div>
							<div class="item">
								<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp mb-20">
								<div class="carousel-caption">
									Descriere imagine
								</div>
							</div>
							<div class="item">
								<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp mb-20">
								<div class="carousel-caption">
									Descriere imagine
								</div>
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="wrapper bg-white-9 mb-20">
		<div class="content-title">
			<h2 class="title">Passport to comfort</h2>
		</div>

		<div class="content-box bg-white">
			<div class="row">
				<div class="col-md-6">
					<p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim. Etiam sit amet justo diam. Aenean enim erat, luctus eu commodo ac, vulputate vel dolor. Duis finibus nulla at congue finibus. Aenean rutrum ipsum nec ultricies sagittis. Duis ac faucibus elit. Integer at venenatis dui. Morbi ut molestie nisi. Duis egestas ex et enim laoreet, venenatis blandit ipsum dictum. Duis consectetur viverra molestie. Nulla mattis sed ligula ut aliquam. Proin sapien tortor, sollicitudin id mi euismod, ornare convallis libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus magna lectus, ullamcorper eu dignissim eget, ullamcorper quis ex. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
				</div>
				<div class="col-md-6">
					<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp">
				</div>
			</div>
		</div>
	</div>

	<div class="wrapper bg-white-9 mb-20">
		<div class="content-title">
			<h2 class="title">Passport to comfort</h2>
		</div>

		<div class="content-box bg-white">
			<div class="row">
				<div class="col-md-4">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim. Etiam sit amet justo diam. Aenean enim erat, luctus eu commodo ac, vulputate vel dolor. Duis finibus nulla at congue finibus. Aenean rutrum ipsum nec ultricies sagittis. Duis ac faucibus elit. Integer at venenatis dui. Morbi ut molestie nisi. Duis egestas ex et enim laoreet, venenatis blandit ipsum dictum. </p>
				</div>
				<div class="col-md-4">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim. Etiam sit amet justo diam. Aenean enim erat, luctus eu commodo ac, vulputate vel dolor. Duis finibus nulla at congue finibus. Aenean rutrum ipsum nec ultricies sagittis. Duis ac faucibus elit. Integer at venenatis dui. Morbi ut molestie nisi. Duis egestas ex et enim laoreet, venenatis blandit ipsum dictum. </p>
				</div>
				<div class="col-md-4">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim. Etiam sit amet justo diam. Aenean enim erat, luctus eu commodo ac, vulputate vel dolor. Duis finibus nulla at congue finibus. Aenean rutrum ipsum nec ultricies sagittis. Duis ac faucibus elit. Integer at venenatis dui. Morbi ut molestie nisi. Duis egestas ex et enim laoreet, venenatis blandit ipsum dictum. </p>
				</div>
			</div>
		</div>
	</div>

	<div class="wrapper bg-white-9 mb-20">
		<div class="content-title">
			<h2 class="title">Passport to comfort</h2>
		</div>

		<div class="content-box bg-white">
			<div class="row">
				<div class="col-md-4">
					<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp mb-20">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim.  </p>
					<a href="" class="btn btn-orange btn-lg btn-block">Buton</a>

				</div>
				<div class="col-md-4">
					<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp mb-20">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim.  </p>
					<a href="" class="btn btn-orange btn-lg btn-block">Buton</a>

				</div>
				<div class="col-md-4">
					<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp mb-20">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim.  </p>
					<a href="" class="btn btn-orange btn-lg btn-block">Buton</a>

				</div>
			</div>
		</div>
	</div>

	<div class="wrapper bg-white-9 mb-20">
		<div class="content-title">
			<h2 class="title">Passport to comfort</h2>
		</div>

		<div class="content-box bg-white">
			<div class="row">
				<div class="col-md-12">
					<img src="{{ asset('images/rooms/room1.jpg') }}" class="img-resp mb-20">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim.  </p>

					<div class="row">
						<div class="col-sm-4">
							<a href="" class="btn btn-orange btn-lg btn-block">Buton</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="wrapper bg-white-9 mb-20">
		<div class="content-title">
			<h2 class="title">Testimoniale</h2>
		</div>

		<div class="content-box bg-white">
			<div class="row">
				<div class="col-sm-6">
					<div class="col-md-2 p-0 text-right">
						<img src="{{ asset('images/others/quotes.svg') }}" class="icon-quotes">
					</div>
					<div class="col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim.  </p>
						<h4 class="mt-20 text-orange">Nume Prenume</h4>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="col-md-2 p-0 text-right">
						<img src="{{ asset('images/others/quotes.svg') }}" class="icon-quotes">
					</div>
					<div class="col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum, ligula vitae dictum dictum, turpis mauris venenatis enim, quis ornare augue elit tincidunt arcu. Duis ut diam consequat, dignissim massa nec, porta enim.  </p>
						<h4 class="mt-20 text-orange">Nume Prenume</h4>
					</div>
				</div>
			</div>
		</div>

		<div class="bg-white border-top">
			<div class="row">
				<div class="col-sm-12">
					<p class="text-right mt-10 mb-10 mr-20 ">mai mult &nbsp;<span class="glyphicon glyphicon-plus text-orange" aria-hidden="true"></span></p>
				</div>
			</div>
		</div>
	</div>
@stop