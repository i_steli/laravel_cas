-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2018 at 07:13 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelcas`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` float NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `first_name`, `last_name`, `email`, `phone`, `country`, `address`, `city`, `postal_code`, `created_at`, `updated_at`) VALUES
(1, 'Ivan', 'Stelian', 'i_steli@yahoo.com', 721772000, 'Romania', NULL, NULL, 0, '2018-03-03 15:15:16', '2018-03-03 15:15:16'),
(2, 'gfddg', 'fgdgd', 'aa@yahoo.com', 721772000, 'rom', NULL, NULL, 0, '2018-03-03 15:18:17', '2018-03-03 15:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(5, 'mare', '2018-02-12 18:19:21', '2018-02-12 18:19:21'),
(6, 'petrecere', '2018-02-12 18:19:28', '2018-02-12 18:19:28');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image`
--

CREATE TABLE `gallery_image` (
  `id` int(10) NOT NULL,
  `gallery_id` int(10) NOT NULL,
  `image_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_image`
--

INSERT INTO `gallery_image` (`id`, `gallery_id`, `image_id`) VALUES
(46, 6, 3),
(47, 6, 2),
(48, 6, 1),
(49, 5, 11),
(50, 5, 10);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_room`
--

CREATE TABLE `gallery_room` (
  `id` int(10) NOT NULL,
  `room_id` int(10) NOT NULL,
  `gallery_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `file_extension` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `url`, `width`, `height`, `file_size`, `file_extension`, `created_at`, `updated_at`) VALUES
(1, '23517_106210452730919_3788986_n---Copy.jpg', '/photos/23517_106210452730919_3788986_n---Copy.jpg', 604, 453, 41799, 'jpg', '2018-02-11 17:26:25', '2018-02-11 17:26:25'),
(2, '37522_140223939329570_5865493_n.jpg', '/photos/37522_140223939329570_5865493_n.jpg', 380, 465, 43607, 'jpg', '2018-02-11 17:26:25', '2018-02-11 17:26:25'),
(3, '40042_149097515108879_7651505_n.jpg', '/photos/40042_149097515108879_7651505_n.jpg', 720, 540, 51387, 'jpg', '2018-02-11 17:26:25', '2018-02-11 17:26:25'),
(4, '46448_150044735014157_1089252_n.jpg', '/photos/46448_150044735014157_1089252_n.jpg', 516, 332, 48117, 'jpg', '2018-02-11 17:26:25', '2018-02-11 17:26:25'),
(5, '290053_413447168700476_626117444_o.jpg', '/photos/290053_413447168700476_626117444_o.jpg', 2048, 1536, 239533, 'jpg', '2018-02-11 17:26:26', '2018-02-11 17:26:26'),
(6, '294455_2022075950769_849704667_n.jpg', '/photos/294455_2022075950769_849704667_n.jpg', 720, 540, 69981, 'jpg', '2018-02-11 17:26:26', '2018-02-11 17:26:26'),
(7, '297311_282682765083686_589559795_n.jpg', '/photos/297311_282682765083686_589559795_n.jpg', 720, 477, 51565, 'jpg', '2018-02-12 18:18:22', '2018-02-12 18:18:22'),
(8, '298439_282684588416837_92596678_n.jpg', '/photos/298439_282684588416837_92596678_n.jpg', 720, 477, 67244, 'jpg', '2018-02-12 18:18:22', '2018-02-12 18:18:22'),
(9, '299147_282677788417517_1811252782_n.jpg', '/photos/299147_282677788417517_1811252782_n.jpg', 713, 489, 52879, 'jpg', '2018-02-12 18:18:22', '2018-02-12 18:18:22'),
(10, '307358_282683125083650_714706639_n.jpg', '/photos/307358_282683125083650_714706639_n.jpg', 720, 477, 50423, 'jpg', '2018-02-12 18:18:22', '2018-02-12 18:18:22'),
(11, '311836_282683441750285_665833435_n.jpg', '/photos/311836_282683441750285_665833435_n.jpg', 720, 477, 79065, 'jpg', '2018-02-12 18:18:22', '2018-02-12 18:18:22');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1,
  `adults` int(10) NOT NULL,
  `initial_price` float NOT NULL,
  `max_adults` int(10) DEFAULT NULL,
  `price_adults` float NOT NULL,
  `children` int(10) DEFAULT NULL,
  `set_age` int(10) DEFAULT NULL,
  `price_children` float DEFAULT NULL,
  `promotion` int(10) DEFAULT NULL,
  `currency` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `description`, `adults`, `initial_price`, `max_adults`, `price_adults`, `children`, `set_age`, `price_children`, `promotion`, `currency`, `created_at`, `updated_at`) VALUES
(1, 'winter', 'winter', 0, 200, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:26:53', '2018-01-27 18:26:53'),
(2, 'summer', 'summer', 0, 150, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:26:53', '2018-01-27 18:26:53'),
(9, 'winter3', 'winter3', 0, 200, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:57:53', '2018-01-27 18:57:53'),
(10, 'summer3', 'summer3', 0, 150, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:57:53', '2018-01-27 18:57:53'),
(11, 'winter1', 'winter1', 0, 200, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:58:57', '2018-01-27 18:58:57'),
(12, 'summer1', 'summer1', 0, 150, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:58:57', '2018-01-27 18:58:57'),
(13, 'winter2', 'winter2', 0, 200, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:58:57', '2018-01-27 18:58:57'),
(14, 'summer4', 'summer4', 0, 150, 0, 0, NULL, NULL, NULL, 0, 'RON', '2018-01-27 18:58:57', '2018-01-27 18:58:57'),
(15, 'aa', 'dad', 0, 1221, 0, 0, NULL, NULL, NULL, 0, 'Lei', '2018-01-28 21:01:52', '2018-01-28 21:01:52'),
(16, 'sdfs', 'dasd', 0, 111, 0, 0, NULL, NULL, NULL, 0, 'Lei', '2018-01-28 21:07:44', '2018-01-28 21:07:44'),
(17, 'aaa', 'asdasd', 0, 2222, 0, 0, NULL, NULL, NULL, 0, 'Euro', '2018-01-28 21:22:59', '2018-01-28 21:22:59'),
(18, 'test mass update', 'asasdad', 0, 100, 0, 0, NULL, NULL, NULL, 0, 'Euro', '2018-01-28 21:24:47', '2018-01-29 19:18:05'),
(19, 'test', 'super', 1, 80, 2, 0, 1, NULL, 10, 60, 'Euro', '2018-03-05 17:56:26', '2018-03-05 18:49:54'),
(20, 'test_2', NULL, 2, 100, NULL, 20, NULL, NULL, 20, 50, 'Dollar', '2018-03-05 18:40:45', '2018-03-05 18:40:45'),
(21, 'test3', 'hahaha', 2, 100, 1, 0, NULL, 10, 20, 0, 'Lei', '2018-03-05 19:08:43', '2018-03-05 19:10:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_image`
--
ALTER TABLE `gallery_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_room`
--
ALTER TABLE `gallery_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gallery_image`
--
ALTER TABLE `gallery_image`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `gallery_room`
--
ALTER TABLE `gallery_room`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
