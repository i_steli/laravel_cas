$(document).ready(function(){
	var select = (function(){
		//Selectors
		var inputSelect = $('.perPage');
		// functions
		var togglePage = function(){
			$(this).closest('form').trigger('submit');
		};
		//	triggers
		inputSelect.on('change', togglePage)
	})();

	var deleteIndex = (function(){
		// selectors
		var btnSelectAll = $(".toggleCheckbox");
		var inputSelectAll = $(".check-box");
		// functions
		var toggle = function(){
			if($(this).prop("checked") == true){
				inputSelectAll.prop("checked", true)
			}else{
				inputSelectAll.prop("checked", false)
			}
		};
		// triggers
		btnSelectAll.on('click', toggle);
	})();

	var activeOnDelete = (function(){
		// selectors
		var divideImage = $(".divide-image");
		var deleteBtn = $(".delete-btn");
		var inputSelectAll = $(".check-box");
		var btnSelectAll = $(".toggleCheckbox");
		// functions
		var showOnCheck = function(){
			var index = inputSelectAll.index(this);
			console.log($(this).prop("checked"), index);
			if($(this).prop("checked")){
				divideImage.eq(index).addClass("active");
				deleteBtn.eq(index).addClass("active");
			}else{
				divideImage.eq(index).removeClass("active");
				deleteBtn.eq(index).removeClass("active");
			}
		};

		var showOnToggle = function(){
			if($(this).prop("checked")){
				divideImage.addClass("active");
				deleteBtn.addClass("active");
			}else{
				divideImage.removeClass("active");
				deleteBtn.removeClass("active");
			}
		};
		// triggersokpmil
		inputSelectAll.on('click', showOnCheck);
		btnSelectAll.on('click', showOnToggle);
	})();

	var priceCalculator = (function(){
		// selectors
		var initialPrice = $(".initial_price");
		var percentageAdult = $('.price_adults');
		var containerAdultPrice = $(".calculated_price_adult");
		var percentageChildren = $('.price_children');
		var containerChildrenPrice = $(".calculated_price_children");
		var promotion = $(".promotion");
		var finalPrice = $(".final_price");
		var hiddenPrice = $(".hide_final_price");


		// functions
		var calculatorPrice = function(){
			// Adult Price
			var valueInitialPrice = initialPrice.val();
			var valuePercentageAdult = percentageAdult.val();
			var calculatorAdultPrice = parseFloat(valueInitialPrice) + parseFloat((valueInitialPrice*valuePercentageAdult)/100);
			if(isNaN(calculatorAdultPrice)){
				alert('Please insert the initial value');
				initialPrice.focus();
				calculatorAdultPrice = 0;
			}
			containerAdultPrice.text(calculatorAdultPrice);
			// Children Price
			var valuePercentageChildren = percentageChildren.val();
			var calculatorChildrenPrice = parseFloat(calculatorAdultPrice) + parseFloat((calculatorAdultPrice*valuePercentageChildren)/100);
			containerChildrenPrice.text(calculatorChildrenPrice);
			// Price after promotion
			var valuePromotin = promotion.val();
			var calculatorFinalPrice= parseFloat(calculatorChildrenPrice*((100-valuePromotin)/100));
			finalPrice.text(calculatorFinalPrice);
			hiddenPrice.val(calculatorFinalPrice);

		};

		// console.log(Number.isInteger(parseFloat(initialPrice.val())), parseFloat(initialPrice.val()));
		// triggers
		if(Number.isInteger(parseFloat(initialPrice.val()))){
			console.log('text');
			calculatorPrice();
		}

		initialPrice.on('keyup', calculatorPrice);
		percentageAdult.on('keyup', calculatorPrice);
		percentageChildren.on('keyup', calculatorPrice);
		promotion.on('keyup', calculatorPrice);
	})();

	var flatpicker = (function(){
		$(".date").flatpickr({
			mode: "range",
			dateFormat: "Y-m-d",
			conjunction: " :: "
		});
	})();

	var blogMenu = (function(){
	//	Selectors
		var blogContainer  = $('.blog-menu');
		var btnBlog = $('.btn-blog');
		var blogTitle = $('.blog-title');
		var blogWrapper = $('.menu-wrapper');
		var bar1 = $('.bar1');
		var bar2 = $('.bar2');
		var bar3 = $('.bar3');
	//	Functions
		var toggleMenu = function () {
			if(blogWrapper.hasClass('change')){
				bar1.addClass('change');
				bar2.addClass('change');
				bar3.addClass('change');
				blogTitle.addClass('hide');
				blogContainer.addClass('change');
				setTimeout(function(){
					blogWrapper.removeClass('change');
				}, 400);
			}else{
				blogWrapper.addClass('change');
				setTimeout(function(){
					blogContainer.removeClass('change');
					bar1.removeClass('change');
					bar2.removeClass('change');
					bar3.removeClass('change');
					blogTitle.removeClass('hide');

				}, 600);
			}

		};
	//	Triggers
		btnBlog.on('click', toggleMenu)

	})();

	var setDate = (function(){
	//	Selectors
		var getDate = $('.date');
		var setDate = $('.intervalDate');
	//	Functions
		function changeDate(){
			setDate.val(getDate.val());
		}
	//	Triggers
		getDate.on('change', changeDate)
	})();

	/*
	 |--------------------------------------------------------------------------
	 | Password Security
	 |--------------------------------------------------------------------------
	 */
	var strengthPassword = (function(){
		// Set up
		var ratingNumbers = 0;
		var eightStringPoints = 0;
		var stringPoints = 0;
		var numberPoints= 0;
		var capslockNumbers = 0;
		var specialNumbers = 0;
		var maxValue = [10, 12, 14];
		var rating = 0;
		// Selectors
		var password = $('.password');
		var displayRating = $('.rating');
		var displayStrings = $('.strings');
		var graphicPassword = $('.col-rating-default');
		var ratingPassword = $('.col-rating ');
		var setRatingHidden = $('.getRating');
		// Functions
		var graphicPasswordMeeter = function(countStrings, ratingNumbers){
			if(countStrings > 0){
				graphicPassword.removeClass('hide');
			}else{
				graphicPassword.addClass('hide');
			}

			ratingPassword.removeClass();

			for(rating=1; rating<=10; rating++){
				if(ratingNumbers === rating){
					ratingPassword.addClass('col-rating-'+rating);
				}
			}
		};

		var strengthPassword = function(){
			// count the strings / numbers / capsolock / special characters
			var input = password.val();
			var countStrings = input.length;
			// regular expression ^ este o negatie va cauta tot ce nu este intre parantezele drepte si le va anula ""
			// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#special-negated-character-set
			var countNumbers = input.replace(/[^0-9]/g, "").length;
			var countCapslock = input.replace(/[^A-Z]/g, "").length;
			var countSpecial = input.replace(/[^@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, "").length;
			displayStrings.text(countStrings);
			// add 2 if displayStrings is equal with 8
			if(countStrings >= 8){
				eightStringPoints = 2;
			}else{
				eightStringPoints = 0;
			}
			ratingNumbers += eightStringPoints;
			// add 1 point for each element 10, 12, 14 if number of strings are equal with them
			$.each(maxValue, function(index, value){
				console.log(value, countStrings);
				if(countStrings >= value){
					stringPoints = 1;
				}else{
					stringPoints = 0;
				}
				ratingNumbers += stringPoints;
			});
			// add max 1 if countNumbers >= 1 add 2 if countNumbers = 2
			if(countNumbers > 0){
				numberPoints = Math.min(countNumbers, 2)
			}else{
				numberPoints = 0
			}
			ratingNumbers += numberPoints;
			// add 1 if countCapslock = 1
			if(countCapslock >= 1){
				capslockNumbers = 1
			}else{
				capslockNumbers = 0;
			}
			ratingNumbers += capslockNumbers;
			// // add max 1 if countSpecial >= 1 add 2 if countSpecial = 2
			if(countSpecial >=1){
				specialNumbers = Math.min(countSpecial, 2)
			}else{
				specialNumbers = 0;
			}
			ratingNumbers += specialNumbers;

			graphicPasswordMeeter(countStrings, ratingNumbers);

			// display rating for security check in label field and in input type hidden field
			setRatingHidden.val(ratingNumbers);
			displayRating.text(ratingNumbers);
			// Reset
			ratingNumbers = 0;
		};
		// Triggers
		password.on('keyup', strengthPassword);
	})();

	(function (){
	//	Selectors
		var input = $('.password');
		var btnPassword = $('.show-password');
	//	Functions
		var togglePassword = function(){
			(input.attr('type') == 'password') ? input.attr('type', 'text') : input.attr('type', 'password');
		};
	//	Triggers
		btnPassword.on('click', togglePassword);
	})();

//	date picker jquery
	$( ".datepicker" ).datepicker({
		dateFormat: "yy-mm-dd"
	});
});

